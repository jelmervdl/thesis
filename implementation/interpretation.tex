\section{Interpretation}
\label{sec:interpretation}
During parsing, as soon as a rule is completed (i.e. all right hand side clauses of the rule are assigned either the result of another rule, have matched a terminal) a partial interpretation is formed. Each rule contains its own logic to form this interpretation.

The partial interpretations consist of a local part and a context. The local part is the claim (or anything less complex) that the rule results in. The context describes the relations between that claim and any claim related to it, the individuals and all information regarding anaphora resolution, and information for claim de-duplication. The final interpretation of a parse is the partial interpretation of the topmost rule, which is the \emph{argument} rule described by each of the grammars.

When partial interpretations are merged, e.g. when a expanded specific claim is constructed from a specific claim and a supporting claim, the contexts of all involved partial interpretations are combined. During this step the list of individuals is combined as described in section~\ref{sec:anaphora}. The same holds true of the list of claims, described in section~\ref{sec:claimdeduplication}.

\subsection{Specific claims}
\label{sec:interpretspecificclaims}
The rules for \emph{specific_claim} itself do not do much interpreting other than combining the partial interpretations that result from the three parts that make up a claim: the subject, the verb, and the object. In the case of a specific claim the subject is an individual, and information about that individual has to be copied to the new partial interpretation. Since a specific claim itself has no parts that can contain a more complex partial interpretation, the merging of the context of these three components is straightforward.

For an extended specific claim it is a bit more advanced. Extended specific claims are constructed in the recursive grammar as described in section~\ref{sec:recursivegrammar} and consist of a specific claim and zero or more supporting and attacking claims. These supporting and attacking claims themselves can also be extended claims, and their context has to be merged with that of the supported or attacked specific claim.

\begin{exe}
	\ex\label{sensc1} Mary is a suspect because she was seen at the crime scene but she claims she was at home.
	\ex\label{sensc2} * Mary is a suspect
	\ex\label{sensc3} * she was seen at the crime scene but she claims she was at home
	\ex\label{sensc4} * she was seen at the crime scene
	\ex\label{sensc5} * she claims she was at home
\end{exe}

For example, in sentence~\ref{sensc1}, `Mary is a suspect' is a specific claim, supported by the expanded specific claim~\ref{sensc2}, which in its turn is a specific claim `she was seen at the crime scene' which is attacked by `she claims she was at home'. When parsing, the rule that matches part~\ref{sensc2} is completed first, part~\ref{sensc4} is completed second, and the rule for part~\ref{sensc5} third. But only after that, the rule that matches the combination of these last two parts, substring~\ref{sensc3}, completes. In completing this rule, the partial interpretations of parts~\ref{sensc4} and~\ref{sensc5} come together. Here, the `she' in part~\ref{sensc4} is merged with the `she' in part~\ref{sensc5}. Then, the rule that combines parts~\ref{sensc2} and~\ref{sensc3} is completed and the `she' is combined with `Mary'. The exact procedure for matching individuals is described in section~\ref{sec:anaphora}. The rules for merging the partial interpretations of the supporting and attacking claims in a \emph{expanded_specific_claim} is discussed in more detail in the following sections, as the exact behaviour depends on the semantic values encountered. In general, for all specific claims the rules are as straight forward as mentioned here with the exception of the introduction of assumptions and the addition of the actual relations to the context of the partial interpretation.

\subsection{General claims}
\label{sec:interpetgeneralclaims}
The interpretation of general claims or conditional claims generally follows the same lines as the interpretation of specific claims. The main differences are how the relations are constructed, and how explicitly conditional claims are handled.

HASL interprets explicitly conditional statements such as `If something is a man, they are mortal' and general claims such as `All men are mortal' in the same way. In both cases the condition (e.g. being a man) is encoded in its own specific claim with a subject that is shared with the general claim. The subject then works in a way similar to a variable in logic programming, and the unification is done during the anaphora resolution phase. The claims are connected to each other through conditional relations. These function in a similar way as attack and support relations, but are not drawn in the argument graph. Instead, the relations are resolved and the text of the conditional claim is rewritten to state the conditions. If there is only one condition, e.g. being a man, the claim is written as `all men are mortal'. If there are multiple conditions, e.g. something being a tortuous act, the conditions are stated explicitly in the form of `something is a tortuous act if \emph{conditional claim a} and \emph{conditional claim b}.'.

\subsection{Relations}
Relations can exist between claims and claims, and between claims and relations.

These two different types of relations can be linked back to Toulmin's model of arguments. A claim supporting a claim is a more general version of the data supporting a claim in Toulmin's argument diagram, or a backing supporting a warrant.

Note that this comparison stops at the naming convention of the different types of relations: in our graph language a claim can be both a backing for a claim and a rebutter for another in the same graph. In similar fashion, the data can be attacked by a rebutter as well, and such an attack can be supported by a construction similar to that of the warrant \& backing.

\subsection{Support relations}
In the recursive grammar described in section~\ref{sec:recursivegrammar}, \emph{supports} and \emph{attacks} are rules constructed using the \emph{and macro} to cover multiple \emph{support} or \emph{attack} rules. A single occurrence of \emph{support} will match \emph{supports} (i.e. the last rule mentioned in section \ref{sec:andmacro} is applied).

There are \todoinline{roughly} three scenarios on how a claim can be supported by one or more supporting claims. The three sentences~\ref{sensup1}~to~\ref{sensup3} are examples of these three different scenarios. Figures~\ref{fig:sensup1graph}~to~\ref{fig:sensup3graph} show their argument graphs.

\begin{exe}
	\ex\label{sensup1} Socrates is mortal because he is a man and because he has died.
	\ex\label{sensup2} Socrates is mortal because he is a man and men are mortal.
	\ex\label{sensup3} Socrates is mortal because he is a man and he is not invincible.
\end{exe}

\begin{figure}
	\centering
	\begin{minipage}{.3\linewidth}
		\begin{haslpicture}[width=\textwidth]{sensup1}
			c: Socrates has died
			d: Socrates is a man
			e: Socrates is mortal
			f: c supports e
			g: d supports e
		\end{haslpicture}
		\caption{Argument graph for sentence~\ref{sensup1}.}
		\label{fig:sensup1graph}
	\end{minipage}
	\begin{minipage}{.3\linewidth}
		\begin{haslpicture}[width=\textwidth]{sensup2}
			a: Socrates is a man
			b: Men are mortal
			c: Socrates is mortal
			d: a supports c
			e: b supports d
		\end{haslpicture}
		\caption{Argument graph for sentence~\ref{sensup2}.}
		\label{fig:sensup2graph}
	\end{minipage}
	\begin{minipage}{.3\linewidth}
		\begin{haslpicture}[width=\textwidth]{sensup3}
			b: Socrates is no invincible
			c: Socrates is a man
			d: Socrates is mortal
			e: c b supports d
		\end{haslpicture}
		\caption{Argument graph for sentence~\ref{sensup3}.}
		\label{fig:sensup3graph}
	\end{minipage}
\end{figure}

Sentence~\ref{sensup1} shows two claims supporting `Socrates is mortal' independently from each other. Of one of these claims is debunked, the main claim, that Socrates is mortal, is still supported.

Sentence~\ref{sensup2} shows how `Socrates is mortal' is supported by a general claim about men, and a specific claim that \todoinline{in a sense} indicates that this general claim is applicable to this scenario. Without the general claim the support remains. \todoinline{This is done quite often in arguments, and this phenomenon is called enthymemes.}

\todo{What is an enthymeme? A syllogism with an unstated premise.}

Sentence~\ref{sensup3} shows how a combination of two claims together can support the main claim. In a sense this is also an enthymeme, the major claim here being that for a man to be mortal, he is to be both a man and not be invincible. If one of both supporting claims is attacked or debunked, the complete support for the claim that Socrates is mortal is compromised.

Besides these three scenarios, there are two types of supporting relations that are treated differently from each other in HASL: claims that support other claims, and claims that support relations. 

\subsubsection{Support of a claim (backing)}
The backing of a claim is the most basic form of a support relation. The following to sentences are examples that can be interpreted as arguments having only this type of relation:

\begin{exe}
	\ex\label{grambacking1} Socrates is mortal because he is a man.
	\ex\label{grambacking2} The act is tortuous because it is a violation of someone else their rights, it is not justifiable and it is a result from the tortfeasor's fault.
	\ex\label{grambacking3} The man is free because the act is not punishable and because he is not a suspect.
\end{exe}

\noindent Examples \ref{grambacking2} and \ref{grambacking3} show that a claim can be backed by multiple backings in two different ways.

In the first case the two backing claims are combined to form a single supporting argument. This is often the case when each of those claims is necessary to make the supporting claim complete, e.g. because there is a rule applicable that is not explicitly mentioned. In example \ref{grambacking2}, leaving one of the three individual claims out would make the backing claim incomplete. Additionally, as a result, one could attack the supporting relation between the claim and the combined backing itself, which would affect the effect of all three claims at the same time.

In example \ref{grambacking3} there are two separate arguments for why the man is free. Attacking the claim that `he is not a suspect' will have no effect on the other supporting claim, that `the act is not punishable'.

Example sentence~\ref{grambacking1} is parsed via the \emph{expanded_specific_claim} rule that matches with the \emph{supports} rule that has one \emph{support} element. At the moment the \emph{expanded_specific_claim} rule if finished, the partial interpretation of the \emph{support} is merged with the \emph{specific_claim} that matched `Socrates is mortal'. At that moment, the combined partial interpretation gains a new support relation which links `he is a man' to `Socrates is mortal'. Additionally, because there is no general claim, a general claim is constructed and marked as assumed. This way HASL visualizes assumed general claims \todoinline{enthymemes}. The construction of this assumed general claim is described in section~\ref{sec:assumptions}.

\subsubsection{Support of a relation (warrant)}
In Toulmin's argument diagram, the warrant supports the relation between the data and the claim. In a sense, it is backing the relation itself. The following examples contain such a relation:

\begin{exe}
	\ex\label{gramwarrant1} Socrates is mortal because all men are mortal.
	\ex\label{gramwarrant2} Socrates is mortal because all men are mortal and he is a man.
	\ex\label{gramwarrant3} Socrates is mortal because he is a man and all men are mortal.
\end{exe}

\begin{figure}
	\begin{minipage}{0.45\linewidth}
		\begin{haslpicture}[width=0.5\textwidth]{gramwarrantassume}
			a: Socrates is mortal
			b: assume He is a man
			c: All men are mortal
			x: assume b supports a
			y: c supports x
		\end{haslpicture}
		\caption{Argument graph for `Socrates is mortal because all men are mortal.'}
		\label{fig:gramwarrantassume}
	\end{minipage}
	\begin{minipage}{0.45\linewidth}
		\begin{haslpicture}[width=0.5\textwidth]{gramwarrant}
			a: Socrates is mortal
			b: He is a man
			c: All men are mortal
			x: b supports a
			y: c supports x
		\end{haslpicture}
		\caption{Argument graph for `Socrates is mortal because all men are mortal and he is a man.'}
		\label{fig:gramwarrant}
	\end{minipage}
\end{figure}

\noindent The warrant is similar to the syllogism in that it always consists of a major and a minor premise that together support a claim. In the examples above, `Socrates is mortal' is the claim, and `all men are mortal' and `he is a man' are the major and minor premise.

The difference between example \ref{gramwarrant1} and \ref{gramwarrant2} is that in the first example the minor premise is left out. Leaving out this minor premise makes it impossible to draw the graph as in figure \ref{fig:gramwarrant} because there is no relation to connect to for the arrow originating from the major premise.

A logical option would be to not allow sentences such as example sentence \ref{gramwarrant1}. However, leaving out the minor premise is not uncommon. \todoinline{Again, mention enthymemes.} To allow you to construct these graphs without being explicit, HASL will assume the existence of the minor premise, and add an assumed specific claim with an assumed supporting relation to the main claim to the graph. In the case of sentence \ref{gramwarrant1} this will result in the assumption `Socrates is a man'. The assumed claim is constructed by taking the subject of the main claim, in this case `Socrates', and the verb and object from the verb and subject of the general claim, `is a man'. Note that when the subject is morphed into the object of the assumed claim, the grammatical number is matched with that of the subject of the main claim. Hence, `all men' becomes `a man' because `Socrates' is singular.

This assumed claim and the assumed supporting relation are drawn in a lighter shade to indicate that they are not explicitly stated, but part of the argument. The actual graph for sentence \ref{gramwarrant1} can be seen in figure \ref{fig:gramwarrantassume}. The exact rules for assumptions are discussed in section~\ref{sec:assumptions}.

As shown with sentence \ref{gramwarrant1} general and specific claims can be mixed. Note that the general claim can only occur once in the \emph{support} rule, either as the first or the last claim. This allows for sentences like \ref{gramwarrant2} and \ref{gramwarrant3}, with are both interpreted in the same way, and thus result in the same argument graph as seen in figure~\ref{fig:gramwarrant}

\subsection{Attack}
Like support relations, attack relations can exist in two forms. They can either exist between two claims, when they describe how one argument counters another argument, or they attack another relation, for example because the reasoning behind that relation is unsound, incomplete or not applicable.

\begin{exe}
	\ex\label{gramattackclaim}Alice is innocent but she was seen at the crime scene.
	\ex\label{gramattackrelation}The house should be demolished because it contains asbestos but the asbestos is easily removable.
\end{exe}

\noindent In HASL, attack relations are recognized by the words `but' and `except'. HASL is not capable of making the distinction between the two different forms unlike for the support relations, mainly because the distinction is more deeply hidden in the semantics. For the support relations the distinction can for many cases be made based on the structure of the claims, i.e. is it a specific claim or a general claim, which is determined based on whether the claim is about an instance or about a category or prototype. For the attacking relations this method does not work, as there is much less a universal structure.

\subsubsection{Attack on a claim (rebuttal)}
Sentence~\ref{gramattackclaim} is an example of the first type, where the claim `Alice is innocent' is attacked by the claim that `She was seen at the crime scene', which can be interpreted as an argument for that Alice might not be innocent. \todo{I still want to discuss that this is an abbreviated version of `Alice is innocent but Alice is not innocent because she was seen at the crime scene', and I'm curious if I can come up with a counterexample for where it is not either such an abbreviation or the contradiction case used in the abbreviation (Alice is vs Alice is not).}

\subsubsection{Attack on a relation (undercutter)}
In example sentence~\ref{gramattackrelation} the attacking relation is between `The asbestos is removable' and `The house should be demolished because it contains asbestos'. Here the claims that `The house should be demolished' and that `The house contains asbestos' are not disputed directly questioned by `The asbestos is easily removable'. If you leave either of the claims out of the argument, the claim that `The asbestos is easily removable' looks out of context. \todo{Let's discuss this can of worms over dinner in another section}

\subsection{Assumptions}
\label{sec:assumptions}
\begin{exe}
	\ex\label{gramwarrantrule} The act is tortious because an act is tortious when it is a violation, it is not justifiable and it is a result.
\end{exe}

\begin{figure}
	\begin{haslpicture}[width=\textwidth]{gramwarrantrule}
		a: assume The act is not justifiable
		b: assume The act is a result
		c: assume The act is a violation
		d: assume The act is an act
		e: Something is tortious if something is an act, the act is a violation, the act is no justifiable and the act is a result
		f: The act is tortious
		g: assume a b c d supports f
		h: e supports g
	\end{haslpicture}
	\label{fig:gramwarrantrule}
	\caption{Argument graph for sentence \ref{gramwarrantrule} showing separated assumed specific claims for each condition in the general claim.}
\end{figure}

\noindent Some general claims can be interpreted as rules, for example legal law. Sentence \ref{gramwarrantrule} is an example of a claim that has only a single supporting claim which is a general or major claim and the warrant for the claim that this act is tortious: it describes tortious acts in general. But for this to be the case the act has to meet three conditions mentioned in the major claim.

Because a warrant is a supporting argument for a relation, there has to be a relation to support and a specific claim (the data) is assumed. Or more exactly, as can be seen in figure \ref{fig:gramwarrantrule} , for every condition a separate specific claim is assumed, and these together form the data for the claim that the act is tortuous. This can be done because the grammar for these rule-like claims allows multiple conditions through the \emph{and macro}.

Internally, these conditions are attached to the major claim (in our example `an act is tortuous') through a \emph{condition} relation, which is similar to an attack or support relation. This is also done for general claims in the form `All men are mortal', which is represented as `Something is mortal' with the conditional claim that `Something is a man'. As a result, when we have sentence \ref{gramwarrant1}, generating the assumed specific claim `Socrates is a man' is done based on all condition relations of the general claim `All men are mortal', which in this case is only `Something is a man'. The subject is matched with that of the main claim, `Socrates is mortal', and the resulting claim, `Socrates is a man' and the supporting relation is assumed.

These condition relations are not drawn and the condition claims are transformed back into the text of the general claim when displaying the argument graph because they are not part of the argument graph language. \todo{However, it is a nice example of how the argument graph can function as a basis to which more semantic data for the argument can be attached.} \todo{Why don't we draw the conditions of a general claim in the graph? Because they are drawn the same way as the rest of the argument graph but their meaning is different.}

If an assumed claim, for example `the act is not justifiable', is explicitly mentioned as a supporting argument, or elsewhere, it will stop to be assumed and be drawn as any other stated claim. Any missing claims or relations will still be assumed.

\subsection{Scopes}
The condition claims in rules make use of the \emph{individual} atom in the subject slot of a claim to connect them to each other. To make the sentences readable the noun `something' or `all' is used to name these singular and plural individuals. During the anaphora resolution step this can cause problems, as the `something' from one of these individuals might be linked to the `something' from another one, as would normally happen when individuals repeat the same noun. To counter this individuals inside general claims are assigned a scope, and the scope is limited to the general claim. Individuals with a different scope cannot be merged. \todo{This sounds broken to me.}

\begin{exe}
	\ex\label{gramscopes} Tweety can fly because a bird can fly if it has wings and Ducky can fly because he has wings.
\end{exe}

\subsection{Position of general claim}
As stated in section~\ref{sec:recursivegrammar}, a general or conditional claim can only occur as the first or last claim in a sequence of supporting or attacking claims. This design decision was made to keep the HASL language close to the English language and the logic behind the parser rules simple. Sentences~\ref{gramwarrant4}~to~\ref{gramwarrant7} are examples of the different orders allowed. All of these are correctly parsed by HASL, except for sentence~\ref{gramwarrant6}. In that sentence the general claim occurs in the middle of the series of supporting claims. \todo{It might also be nice for future development as when you encounter a general argument not at the beginning or the end, like in `X because A1, A2, AG and B1', it often means that a second series of arguments starts that are unrelated. Currently in HASL you can only separate arguments by repeating `because'. Detecting these more subtle series could be an additional way to express them.}

\begin{exe}
	\ex\label{gramwarrant4} Socrates is mortal because he has died, he is a man and all men are mortal.
	\ex\label{gramwarrant5} Socrates is mortal because he is a man, he has died and all men are mortal.
	\ex\label{gramwarrant6} * Socrates is mortal because he is a man, all men are mortal and he has died.
	\ex\label{gramwarrant7} Socrates is mortal because he is a man and all men are mortal and because he has died.
\end{exe}

\subsection{Negations}
A side effect of this recursive definition of negation is that it automatically deals with claims like `He is not not nice'. The interpretation function of this rule marks the object (the second word that matches category, prototype, prototypes or verb_inf) as negated unless it is already marked as such. In that case it will remove the mark.

During the claim de-duplication step when combining arguments, claims that have the same subject and verb, but of which the object of one is the negation of the other, are linked together with a symmetric attack relation.

\todo{This rule does not follow conventional English grammar, and as a result you get some weird stuff. Not every place where you can write `a car' you can also write `not a car'.}

\todo{Negations are limited to these explicit forms. For example, `Socrates is not mortal because few men are mortal' is an interesting case. `few men are' implies `many are not'. That is not an assumption that the system can come to right now. Another one of these, `Socrates is not mortal because only men are mortal' will not make the assumption that Socrates is not a man.}

\todo{Sentences like `Dirk can not fly because no car can fly.' don't actually work, unfortunately.}

\subsection{Instances}
\label{sec:instances}
Each instance has a few properties to keep track of: a name, a pronoun, and a noun. \todoinline{Next to these the instance also has a scope and an id, but those are more implementation details than anything else.} Each instance has to have at least one of these properties, but by merging instances that are believed to refer to the same subject, they can gain more of them. But every property can only have one value. As a result, `Elizabeth II' and `the queen' can refer to the same instance, but `the queen' and `the empress' cannot, because an instance can not be associated with two nouns.

\todo{Why are instances treated differently from categories and prototypes?}

\todo{What about instance groups? Well, those are barely tested. But they are implemented! And arguments can be about more than one subject! Well, technically, a group is still a single subject.}

\subsection{Anaphora}
\label{sec:anaphora}
Anaphora resolution is done each time partial interpretations are combined, i.e. when a rule is completed.

Each partial interpretation keeps a list of all distinct instances, and for each of these instances a list of occurrences in claims. These lists can be used to identify which current instance is connected to the instance observed in the various claims.

For example, in the sentence `Socrates is mortal because he is a man.', there is only one distinct instance in the final interpretation, namely Socrates. While parsing, both `Socrates' and `he' are observed as instances. So in the final interpretation, the list of observed instances associated with `Socrates' contains both these. By merging these, which in this example happens when the \emph{expanded_specific_claim} rule finishes, a new instance is created which contains both the name `Socrates' and the pronoun `he'. When drawing the graph, the terminals `Socrates' and `he' can be looked up in the lists and it can be determined that these refer to the same subject. This can then be displayed in the graph (e.g. by associating and printing the same number next to each terminal that refers to this instance).

When merging partial interpretations, if two separate instances are believed to be referring to the same subject, a new canonical instance is created and added to the list with distinct, canonical instances. Then, this new instance is associated with the two merged (previously canonical) instances and all other (non-canonical) instances that were already associated with these two.

For anaphora resolution it is important to know which of the instances came first in the order of the words of a sentence. For example, it is likely that in the sentence `John said he saw a deer.' `he' refers to `John', but in the sentence `He said John saw a deer' `He' is probably not referring to `John'.

The following conditions determine whether two instances can be merged. These conditions are evaluated from first to last. \emph{First} refers to the first occurring instance in the order of the sentence, and \emph{second} to the second.

\begin{enumerate}
	\item if at least one of the instances has a scope and these scopes are not equal, they cannot be merged.
	\item if the first instance has the pronoun `something' and the second has the pronoun `it', they can be merged.
	\item if the first has the pronoun `someone' and the second has the pronoun `he' or `she', they can be merged.
	\item if the first and the second have the same name, they can be merged.
	\item if the first has a name and the second does not have a name and either the first has no pronoun and the second has `he' or `she' as pronoun, or the pronouns of the first and second are equal, they can be merged. Otherwise, if the first does have a name, they cannot be merged.
	\item if the first has a noun and the second has the same noun, or does not have a noun but has a pronoun `he', `she', or `it', they can be merged.
	\item if the first has the pronoun it, they can be merged only if the second also has the pronoun it.
	\item if the first has the pronoun `he' or `she', they can only be merged if the second has the same pronoun.
	\item Otherwise they cannot be merged.
\end{enumerate}

\todo{Maybe instead of writing rules, make a lookup matrix of when the first and the second can be merged. Values in the cells of such a matrix would be `yes', `no', `if equal', `only if equal' or `continue'. Can this not be described simpler?}

\todo{For each of these rules, describe an example. Should be simple enough. But what does that show? Maybe add multiple examples for some, showing why it should be true when true and fail when false, etc.}

\subsection{Claim de-duplication}
\label{sec:claimdeduplication}
Claim de-duplication also happens during the combining of partial interpretations, and happens in a similar fashion as anaphora resolution. A list with canonical claims is kept per interpretation, and for each canonical claim a list with all occurrences of that claim is kept.

Claim de-duplication is simpler, and happens by comparing the scope, the verb and the object of the claim. For the subject the canonical version of the subject is looked up in the instance list of the current partial interpretation before comparing it to the subject of the other claim. When merging general claims, where the subject is not an instance, they are considered equal when their words are equal.

In some cases a claim is duplicated in an argument because the claim was first assumed and only occurs later on in the argument. An example of such a scenario would be sentence~\ref{senassdedup}. This sentence incorrectly states that `he is a man' is a separate supporting argument, apart from `all men are mortal'. For the `all men are mortal' relation to be constructed, the assumed specific claim `Socrates is a man' is assumed in the interpretation. Similarly, for the claim that Scorates is mortal `because he is a man', the general claim that men are mortal is assumed. Both these assumed claims now both occur as assumed claims and as observed claims. In this case, the observed claim is merged with the assumed claim, and the assumed claim is no longer drawn in the graph as assumed. \todo{But what about the relations that were assumed to connect these assumed claims. Are those still assumed?}

\begin{exe}
	\ex\label{senassdedup} Socrates is mortal because all men are mortal and because he is a man.
\end{exe}