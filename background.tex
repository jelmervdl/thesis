\section{Background}
For a computational understanding of argumentative texts, two research topics need to be combined: what is the structure of an argument, and how can we extract structural arguments from text. The choices made in answering the first question define the best course of action for the second.

\subsection{Argument structure}
That there is structure in arguments is undisputed: statements can be supported by arguments. The amount of structure, and the strictness of it, is what the real discussion is about \citep{vanEemerenEtal2014}. 
One simple but effective abstraction of the structure in argumentation is that of the syllogism, as described by Aristotle.

\paragraph{Syllogism}
Aristotle's syllogism is the prime example of structure in argumentation. Take the argument `Socrates is mortal because Socrates is a man and men are mortal.' Or, transcribed in the classical form:

\begin{quotation}
\noindent Aristotle is a man (minor)\\
Men all mortal (major)\\
---\\
Socrates is mortal (conclusion)
\end{quotation}

\noindent A syllogism always consists of a minor premise, which is what we expressed to be a singular claim and a major premise, which is a more general claim, and finally a conclusion. This conclusion describes the general phenomenon described in the major premise applied to the subject of the minor premise.

This structural description of an argument is not very refined, and as such it may be more interesting to search for a structure that can highlight more specific parts in an argument. More structure provides a better framework for working with arguments when, for example, evaluating them or when identifying which premises are explicitly stated and which are left implicit.

\paragraph{Toulmin}
Toulmin's argument model \citep{Toulmin:2003ec}, shown in \autoref{fig:toulmin}, follows the form of the syllogism, but defines in more detail what the role of the premises can be in an argument, and how to deal with attacking claims. It also expands the number of elements, as it sees the need for more information than a syllogism can hold to describe an argument. The model, shown in \autoref{fig:toulmin}, consists of five elements: the \emph{claim}, \emph{datum} and \emph{warrant}, which are respectively comparable to the conclusion, and the minor and major premise in a syllogism, and the \emph{backing} and \emph{rebuttal}. % Yes I'm ignoring the qualifier

\begin{figure}
	\centering
	\begin{subfigure}[b]{0.45\textwidth}
		\includegraphics[scale=0.4]{argumentation/toulmin.pdf}
		\caption{Elements of the model.}
	\end{subfigure}\hfill
	\begin{subfigure}[b]{0.5\textwidth}
		\begin{haslpicture}[scale=0.5]{toulminexample}
			style claim.maxWidth 100
			a: Harry is a British subject
			b: Harry was born in Bermuda
			c: A man born in Bermuda will be a British subject
			d: He has become a naturalised American
			e: The following statutes and other legal provisions
			f: b supports a
			g: c supports f
			h: e supports c
			i: d attacks a
			position a at 184 0
			position b at 0 0
			position c at 92 67
			position d at 218 69
			position e at 100 186
		\end{haslpicture}
		\caption{Example of an argument in this model.}
		\label{fig:bgtoulminexample}
	\end{subfigure}
	\caption{\citet{Toulmin:2003ec}'s argument model.}
	\label{fig:toulmin}
\end{figure}

The \emph{warrant} serves the role of telling why jumping from the datum to the claim is justified. The warrant is universal, the datum and claim are specific to the scenario described in the argument. This matches our concept of a general rule, or a general claim.
The warrant itself can be supported by a \emph{backing}, which is a claim that is not specific to the argument at hand, and thus has barely anything to do with the \emph{conclusion}, but it is often very specific to a field. For example, the warrant in the example shown in \autoref{fig:bgtoulminexample}, that a man born in Bermuda will be a British subject, is supported by legal reasons, as opposed to for example statistical (`95\% of all people born in Bermuda are a British subject') or empirical reasons (`Everyone I know born in Bermuda is a British subject').

Many general rules have exceptions, but these exceptions are only mentioned when applicable in a discussion. For example, the general statement that `birds can fly' has many exceptions, such as the bird being a penguin. In a syllogism there is no clear place for such exceptions, and they are often treated either as a conditional statement in the major premise, or as major premise that reaches the opposite conclusion (i.e. `penguins cannot fly') on its own.
In the first approach, the distinction between a condition and an exception is lost, even though there is a clear distinction in argumentation: a condition needs to be met (e.g. there needs to be evidence to support this) before the conclusion of the major premise is assumed, while an exception is often not even mentioned, unless it is applicable. Then, both the exception itself and the supporting evidence that the exception is applicable are presented. % TODO: any references to the distinction between conditions and exceptions? Pollock?

Toulmin allows for exceptions to enter the argument, or any attack to the conclusion, in the form of the \emph{rebuttal}: a claim that can give a reason why the conclusion is not true in this particular scenario.

This makes clear that Toulmin's argument model assumes that the warrant, backing, etc. are not actively reasoned about. It is a model of the correct finalized argument. If the backing itself would be argued by someone, that itself would form a new argument with its own diagram. As such, this model is not mend to show opposing views in the same diagram, or highlight all considerations. It focusses on what elements are essential in a complete and concluded reasoning step inside a possibly larger argument. %TODO: Gaat Verheij's review van Toulmin hierover?

\paragraph{Combining premises}
Toulmin's model does not take the composition of arguments into account; many small arguments often are part of a greater argument, for example a court case or a political debate.

An overview of such a large scale argument has been covered by argument maps, such as the ones developed by \citet{Wigmore:1913pr}, shown in \autoref{fig:wigmorechart}. The squares and circles signify arguments supporting and attacking certain claims: whether a premise supports of attacks is a property of the premise itself. Hence the structure has to be a tree (or multiple trees) where arguments can only attack or support a single other argument higher-up.

\begin{figure}[h]
	\centering
	\includegraphics[scale=0.5]{wigmore-chart.png}
	\caption{Example of a Wigmore chart, taken from \citet{Wigmore:1923tr}.}
	\label{fig:wigmorechart}
\end{figure}

\citet{Grewendorf:1980uh} moves the signalling of support or attack to the arrow between arguments, which now allows the schema to form a graph instead of a tree, where arguments can attack multiple other arguments while also supporting other arguments.

\citet{Thomas:1981} continued this use of argument graphs, but made a clear distinction in how two claims support a conclusion: either cooperatively, or independently.
Arguments can support each other in different combinations. For example, some general rule-like claims assume that multiple conditions are met, e.g. `it snowed outside when it rained and the temperature was below zero'. Only when both conditions can be supported, this general claim would strengthen the conclusion that `it snowed'. In this case, the two claims \emph{cooperatively support} the conclusion.
On the other hand, often there are multiple but separate reasons supporting an argument, for example `I see snow on the rooftops' and `the man who just came from outside was covered in snow' are two separate reasons to assume that the conclusion `it snowed' is true: These two claims \emph{independently support} the conclusion.

These argument charts and graphs all do not have the clear distinct components from Toulmin's model. \citet{Freeman:1991} combines the descriptiveness of Toulmin's model with the freedom to compose arguments introduced with the argument graphs, and refines the meaning of the rebuttal, making a distinction between the different types of attacking arguments.

\citet{Peldszus:2013jw} in turn simplify the schema proposed by Freeman, letting instead the structure of the graph be more defining for the meaning of the components: a conclusion is a box that is supported, an attack is a box that is the source of an attack relation. Claims can support or attack other claims cooperatively or independent.

Their schema does not have a specific place for Toulmin's warrant or Pollock's undercutter. \citet{Verheij:2003gx}'s \textsc{DefLog} can express these by not only allowing relations to target other claims, but also other relations. In this way a warrant is expressed as a support for the support relation between Toulmin's datum and conclusion \citep{Verheij:2005}. Verheij expresses these relations graphically in his tool \textsc{ArguMed 3} by drawing an arrow to the line of the supported or attacked relation (\autoref{fig:deflogtoulmin}).

\begin{figure}
	\centering
	\includegraphics[width=8cm]{background/verheij2005-deflog-toulmin.png}
	\caption{Toulmin's model drawn using \textsc{DefLog}'s model. This representation includes evaluation information. Taken from \citet{Verheij:2005}.}
	\label{fig:deflogtoulmin}
\end{figure}

In Verheij's model the relation originating from the warrant can also be supported, and that relation as well, etcetera. He does not define how such relations should be interpreted, only how such arguments can be evaluated.

\subsubsection{Argument evaluation}
The simple and well defined structure of argument graphs forms a good starting point for the computational evaluation of arguments. As opposed to the syllogism, there is no need to comprehend the exact meaning of the text attached to the nodes in the argument graph in order to evaluate whether a conclusion is supported, under attack, or undecided. Of course, for any meaningful evaluation such understanding is necessary, but these tasks can be well separated.

HASL does not do evaluation, but the evaluation of and reasoning with arguments would be a logical extension of HASL: the purpose of understanding arguments in argumentative text is to reason with it in a later phase.

\paragraph{Default Logic} A starting point for reasoning with the same semantics as arguments is a logic for default reasoning \citep{Reiter:1980ix}. Default reasoning allows us to differentiate conditions from exceptions. For example, a condition for an animal to be able to fly might be that it needs to be a bird. An exception to this rule might be that the bird cannot be a penguin. If this would need to be expressed in first-order logic, we would need to define an implication that on the condition side would need to explicitly state all exceptions as predicates that may not be true. If we were to evaluate such an implication, we would need to find a truth value for each of these exceptions. I.e. we would need to test whether the bird is not a penguin, an ostrich, etc.

Default logic allows us to express the implications without mentioning all of the exceptions by making the implications a bit weaker: the implications gain an extra condition, one of consistency. I.e. a bird is able to fly unless that is inconsistent with what is known. This allows the usage of separate implications for exceptions, i.e. penguins cannot fly. If a bird is then a penguin, it cannot fly as the assumption that it can fly because it is a bird is inconsistent with the conclusion of the implication that says that penguins cannot fly.

To interpret this in the context of argumentation, exceptions do not have to be made explicit, unless they are the case, which is often the case in argumentation since there is a urgency to only express what is relevant due to the need for communication to be efficient.

This is the start for a formalism for the non-monotonicity of the logic behind arguments: the introduction of a new claim may not change the evaluation value of any of the previous claims directly, but it can introduce a new path to reason along which would result in a new valuation for certain claims. This, essentially, is the act of arguing: by introducing new claims, the argument moves along.

But that does not cover the idea that arguing often starts with a conclusion, and then states the arguments in supports of that conclusion, after which the task to attack this reasoning to another party in the argument. The defeasible reasoning described by \citet{Pollock:1987jx} is closer to the argument structures previously described: Instead of allowing each logical statement to have exceptions, i.e. conditions which when met retract the conclusion of the statement, Pollock defines a second type of rules: prima facie reasons, which are true, unless they are not. This matches closely with  general rules such as `birds can fly', which are true unless more information tells us they are not in a particular scenario. 

\citet{Verheij:2003gx}'s \textsc{DefLog} builds on this idea of defeasible reasoning in its evaluation schema. Constants (i.e. claims) are assumed to be justified unless they are defeated, and become justified again when this `suppression' it of their justification is lifted. This allows \textsc{DefLog} to express sets of assumptions (i.e. arguments with claims and attack and support relations between them) and then evaluate how these arguments can be interpreted, which claims can be assumed to be true at the same time.

A different view on defeat is offered by attack graphs \citep{Dung:1995dsa}. This idea matches the idea of an argument as a graph, except that an attack graph only consists of arguments that attack each other. Inside each argument the supporting arguments can still play a role in determining whether that particular node in the graph, that particular argument, is considered to be true or false. This view can be expanded to include monotonic logic and to model the different forms of attacking an argument described by Pollock and Freeman \citep{Besnard:2014bs}.

To conclude, for a structural representation of argumentation that can be used to describe and evaluate argumentation, we need defeasibile rules, and we need to express reasoning about such rules. Furthermore we need to accept that argumentation is a process, and as such the set of arguments that are accepted at one moment may change at any moment new information becomes available.
The necessary elements for such argument structures are defined by \cite{Vreeswijk:1997fw} in order for them to support evaluation.

% A different issue which occurs when evaluating arguments is the occurrence of contradicting arguments. Verheij says defeating arguments win. Vreeswijk proposes a ranking.

\subsubsection{Argument schemes}
The previous sections mostly regarded how ideal argumentation should be structured and interpreted, but aside from corpora of annotated arguments \citep{Lawrence:2012wc}, arguments do not occur often in such ideal forms. Argumentation can be found in argumentative texts, which are much less structured. Hence the need for argument mining. However, for understanding the task of argument mining we first need to understand how arguments occur in text.

\citet{Walton:2008vr} started with documenting common structures used in argumentation, marking their elements. An example of such a common structure in an argument would be the sentence:

\begin{displayquote}
	Attacking the Soviet Union lost the war for Germany claims Dr. Brown, and she is an expert on the Second World war.\footnote{Example obtained from \url{https://www.rationaleonline.com/map/96cb3e/argument-from-expert-opinion}}
\end{displayquote}

\noindent In this example, the claim of Dr. Brown about the reason why Germany lost is supported by the claim that she is an expert on the subject. Walton's schema for Argument from Expert Opinion, shown in \autoref{fig:walton}, describes this argument: it describes the premises and the conclusion that need to be present in a general format. All similar sort of arguments follow this schema. As such it could be interpreted as a model like Toulmin's model, except for very specific types of arguments.

Note that, just like with Toulmin's model, not all boxes in \autoref{fig:waltonschema} can directly be filled in based on the short sentence about Dr. Brown. Some of the slots in the schema are left empty as there is no information available in the sentence that would fill them. In this way, Walton's argument schemes also propose critical questions, such as `Is Dr. Brown an expert on the subject?'. Walton, and many others, defined such schemes based on the arguments they encountered. Consequently, there is no complete set of all schemes.

\begin{figure}[h]
	\centering
	\begin{subfigure}[t]{\textwidth}
		\centering
		\begin{haslpicture}[scale=0.45]{waltonschema}
			style claim.maxWidth 150
			a: P
			b: E asserts P
			c: E knows if P is true or not
			d: E would truthfully assert what they know about P
			e: E is an expert on subject Q
			f: P is something an expert on Q would know
			1: b c d supports a
			2: e f supports c
		\end{haslpicture}
		\caption{Walton's schema}
		\label{fig:waltonschema}
	\end{subfigure}

	\begin{subfigure}[t]{\textwidth}
		\centering
		\begin{haslpicture}[scale=0.45]{waltonexample}
			style claim.maxWidth 150
			a: Attacking the Soviet Union lost the war for Germany
			b: Dr Brown asserts that attacking the Soviet Union lost the war for Germany
			c: Dr Brown knows if this is true or not
			d: Dr Brown would truthfully assert what she knows about Germany's defeat
			e: Dr Brown is an expert on WW2
			f: The reason for Germany's defeat is something an expert on WW2 would know
			1: b c d supports a
			2: e f supports c
		\end{haslpicture}
		\caption{Example}
		\label{fig:waltonex}
	\end{subfigure}
	\caption{Walton's schema for Argument from Expert Opinion, drawn according to the structure we will describe in \autoref{sec:argumentation}.}
	\label{fig:walton}
\end{figure}

\subsubsection{Enthymemes}
Walton schemes also provides a base for detecting enthymemes: premises that are not explicitly mentioned, but assumed to be undisputed. Enthymemes are common \citep{Reed:2004tb}, but challenging for a complete understanding (e.g. when an unstated premise is supported or attacked) and the possibility of evaluation.

\citet{Walton:2005dc} discuss using forms of deductive or abductive inference and later argument schemes to identify and add the enthymemes to the argument diagram. This first approach may result in multiple competing interpretations, depending on what can be deduced from the present claims in the argument. Here the principle of charity is a common approach: `When interpreting a text, make the best possible sense of it'. \citeauthor{Walton:2005dc} interpret this as picking the interpretation that makes the argument stronger. However, they say, this might distort the original argument. Similarly, an argument may have a hidden premise that is false, and by making it explicit, an argument would become weaker.

Using argument schemes can be a more general approach to enthymeme resolution. \citeauthor{Walton:2005dc} argue that goal directed practical reasoning is the basis of many enthymemes. For example, arguments based on analogies often imply their conclusion or the claim that both types mentioned in the analogy are comparable. The argument scheme for analogies defines the existence of such claims, and could thus provide a useful basis for enthymeme resolution.

\subsection{Argument software}
Walton's argument schemes describe particular types of naturally occurring arguments, but there is also a need to annotate the structure in specific textual arguments. Rhetorical Structure Theory (RST) is used to annotate the structure present in text. Originally developed for text generation, RST describes the relations between sentence segments \citep{Mann:1988}. In the use case of understanding arguments in text, these relations are often linked to Walton's argument schemes, which then serve as the framework and common ground between different arguments which do follow the same structure.

\cite{Reed:2004tb}'s \textsc{Araucaria} is a tool for annotating these argumentative texts with argumentation schemes. While annotating a text, the argument can be visualised as a diagram for a better understanding. Arguments annotated in \textsc{Araucaria} can be exchanged in the Argument Markup Language (AML) or Argument Interchange Format (AIF), and these can then be shared among researchers using \textsc{AIFdb} \cite{Lawrence:2012wc}.  The latter is a common source of material for machine learning approaches to argument mining, as well as for sharing arguments for educational purposes.

\textsc{Rationale} \citep{vanGelder:2007dg} is another argumentation software that focusses on helping people understand and construct correct arguments. It achieves this by creating a visual syntax, a consistent way of hinting the correct interpretation of the argument diagram using visual cues. It can also guide the evaluation of arguments, by going through it claim by claim with the user.

\begin{figure}[h]
	\centering
	\begin{subfigure}{.45\textwidth}
		\includegraphics[width=\textwidth]{background/reed2004-araucaria.jpg}
		\caption{Screenshot of Araucaria as shown in \cite{Reed:2004tb}.}
	\end{subfigure}
	\begin{subfigure}{.45\textwidth}
		\includegraphics[width=\textwidth]{background/vangelder2007-rationale.png}
		\caption{Screenshot of the visual syntax of Rationale as shown in \citep{vanGelder:2007dg}.}
	\end{subfigure}
\end{figure}

\subsection{Argument mining}
\label{sec:bgargmin}
RST and argumentation schemes offer ways to add structure to arguments in text. Adding this structure to text, either by hand or automatically, is the practise of discourse analysis. Approaches to discourse analysis usually aim at identifying various different types of discourse relations. However, we are only interested in a subset of such relations, namely those relevant for argumentation structure parsing. Argument Mining is this smaller task performed automatically.

\paragraph{Argument models in argument mining}
The argument models used in Argument mining are often specifically designed or adjusted for the data they work with. For example, much of the earlier research is done on corpora annotated using RST, and as such the relations are classified as `attribution', `background', `condition', `elaboration', etc. \citep{Reitter:2003uk,Herneault:2010cg}. More recent work uses more abstract models: \citet{Mochales:2011eg} just identifies premises that support arguments in favour of the conclusion in legal texts, and \citet{Lawrence:2015un} combines support and attack relations with Walton's argumentation schemes to describe arguments that originate from AIFdb, a dataset largely constructed using tools specifically created for annotating arguments with Walton's schemes. Even more recent, \citet{Stab:2017cd} only identifies claims, conclusions (which they call major claims) and premises with support and attack relations between them to describe persuasive essays.

Argument mining approaches is generally split up in three subtasks, often implemented and evaluated independently \citep{Lippi:2015ch}.

\paragraph{Identification} The parts of text that are argumentative –- that may contain arguments -- need to be identified: In many texts only certain sentences or paragraphs of the text are argumentative; the rest may be informative, for example providing context or anecdotes. In its essence this is a binary classification task. Some approaches combine this task with the next and directly detect and classify the argument components \citep{Stab:2017cd}. In this case the classifier is also tasked with detecting the type of component (e.g. conclusion, minor or major premise, etcetera depending on the structural model used.)\\
The identification task is not always necessary. For example, \citet{SaintDizier:2012ko} focusses on technical manuals, which are processed in total, i.e. their whole structure is interpreted and extracted.

\paragraph{Segmentation} The identified parts of argumentative text need to be split into segments -- the task of component boundary detection -- where each segment may be a component in the final argument representation, i.e. a box in an argument diagram. Assuming that in a text about a court case the paragraphs containing the ruling and the reasoning behind it are identified, this step then splits those parts into components (which we refer to as claims, but often also named argumentative discourse units \citep{Cohen:1987tu} or elementary discourse units \citep{SaintDizier:2012ko}) that can then be identified as the conclusion, premises, etc. This, again, is a classification task. \cite{Knott:1994hb} argues for a data-driven search for cue phrases (i.e. discourse markers such as the words `because', `unless', but also words, word phrases and punctuation that are less explicit, or a complete analysis of the semantic structure) that indicate relations between text spans. \citet{Marcu:1997kn} uses regular expressions and procedures manually derived from a corpus analysis. \cite{Herneault:2010cg,Mochales:2011eg,SaintDizier:2012ko,Lawrence:2015un,Stab:2017cd} use models trained on lexicosyntactic features.

\paragraph{Prediction} The relations between the identified components need to be identified. In this task the components are connected to each other to form the structure of the argument. This is the most challenging task as it requires insight in the meaning of a component to determine whether a premise component supports a certain claim component or whether it is related to another claim component. Some approaches also approach this as a binary classification task, testing each of the claim components in a certain defined order \citep{Cohen:1987tu}. Others have a set of hand-coded grammar-like rules combined with constraint satisfaction to identify the structure between the components using discourse markers, certain word orders and other keywords occurring in the components \citep{Mochales:2011eg,SaintDizier:2012ko} Another approach is to create a model that can determine the likelihood that two claims are related to each other and construct multiple possible argument structures to finally select the best overall structure, i.e. the one that is the most complete or has to best overall likelihood. This was done by \citet{Marcu:1997kn} by creating a model for relations based on the discourse markers occurring in both components. Later research uses many more types of features describing the words occurring in the component and the position of the component relative to other components \citep{Herneault:2010cg,Lawrence:2015un,Stab:2017cd}.

Often knowledge about the domain is used to help in this task. For example, for some texts, such as procedures or didactic text, it can be assumed that a claim is always followed by one or more premises supporting that claim. In juristic texts certain marker words may signal the relations between components.

Most approaches are based on text which has been annotated using RST and a predefined set of schemes. The rules used in the three subtasks are, either by hand or automatically, derived from these previously annotated texts to create a model which can apply the same annotation to new texts.

% \citet{Feng:2012vt} has also been capable of parsing argumentative structures in text (using \citet{Herneault:2010cg}'s HILDA) and constructs a tree by combining two segments using a relation each step. Identifying the structure inside sentences proved to be easier than between sentences. Identifying the right type of relations, like \cite{Kang:2014kz}, is more challenging.

% Todo: discourse markers make it doable to parse arguments, but when they are missing or ambiguous it becomes difficult {Moens:2018gd}

\paragraph{Trend towards more general approaches}
Modern argument mining approaches are leaning less on natural language processing tools like parsing and part-of-speech tagging, and are turning to unsupervised learning of the meaning (i.e. common knowledge) and relations (i.e. entailment and causal relations, and eventually argumentative relations) between segments in texts directly from the words and their embeddings \citep{Moens:2018gd}.

% 

% \cite{Kang:2014kz} gives an analysis of argument structure in the linguistic context. \todoinline{Finding argument components and identifying the relations between them.} (also RST)

% \cite{Bos:2011il} gives an overview of a semantic representation of parsed text, on top of first order logic (FOL). Upside is that it can deal with ambiguity, and entailment through model building and automated reasoning. FOL cannot express statements that are usually true, but not always (e.g. prima facie).

% \cite{Marcu:1997kn}, \cite{Mochales:2011eg} parse text into arguments in a similar way: segment them based on marker words, and then construct and identify a structure based on Walton's schemes. In their approach an argument consists of at least two propositions (reason and conclusion) where these follow an argumentation scheme. Unlike the previous two, the arguments are not joined into a grand argument.
