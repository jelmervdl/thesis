\section{Introduction}
% We start of with the five questions

\paragraph{Problem}
Humans communicate and share their knowledge and understanding of the world by arguing with each other: hypotheses are shared, and defended or attacked by other knowledge. Assumptions are made and acknowledged or retracted based on arguments. Elementary but essential questions like `why?' and `how?' are answered with arguments. If computers could understand this process, they could take part, which would be a most natural way of communicating with computers.

Computers essentially reason in formal logic, where data, combined with a rule, leads to a conclusion. If the conclusion is wrong, either the data or the rule was wrong. Or the program contains a bug.

Humans reason differently. They start with the conclusion, an assumption, and then try to find reasons why this assumption should hold or not. These reasons can be generalized rules, assumptions themselves, that work for 90\% of the time, but exceptions are allowed. Exceptions are just more specific generalized rules. The reasoning itself often still contains the same elements of formal logic, but the main difference is that any intermediate conclusion, any assumption, is asserted to be true unless questioned.

\begin{figure}
	\centering
	\begin{haslpicture}[scale=0.5]{introex}
		style claim.maxWidth 180
		c: Doom will be a bad movie
		w: Video game stories make for bad movies
		d: assume Doom is a video game story
		a: Tomb Raider was not that bad
		x: d supports c
		y: w supports x
		z: a attacks w
	\end{haslpicture}
	\haslsentence{Doom will be a bad movie because video game stories make for bad movies, although Tomb Raider was not that bad.}
	\caption{What we aim for: the text is interpreted by our software and the argument structure is filled in.}
\end{figure}

\paragraph{Current work}
As such, arguments need a different structure than just formal logic. This structure of arguments is well explored \citep{vanEemerenEtal2014}. Elements essential to argumentation can be found in formal logics, such as Default Logic \citep{Reiter:1980ix} or Defeasible Logic \citep{Pollock:1987jx}.

But most formalisms treat arguments as a graph structure with claims supporting and attacking each other \citep{Thomas:1981,Mann:1988,Freeman:1991,Peldszus:2013jw}.
Often these supporting and attacking relations are seen as just different types of the same relations, but that is not set in stone. E.g. \citet{Dung:1995dsa} modelled only attacking relations between arguments, where arguments themselves are assertions, optionally supported by other claims, reasoning, evidence, etc.

Arguing is not just the creation of a graph, there are more `rules' to it. Arguments to have a certain structure, elements that are expected to be present, either generally \citep{Toulmin:2003ec,Verheij:2005} or in specific types of arguments \citep{Walton:2008vr}. Likewise, there are expectations on how claims can be attacked, and how these attacks differ semantically \citep{Pollock:1987jx}.

Given a structured argument in which all claims and their relations among each other (i.e. whether they support or attack each other), and a certain order of which claims or relations are stronger, more important, more impressive than others, we can use algorithms to evaluate which claim is accepted as true and which isn't \citep{Reiter:1980ix,Dung:1995dsa,Verheij:2003gx}. Once we have computers that are able to follow along with an argument, we could ask it about its interpretation, and whether it thinks the conclusion is true given its knowledge of some of the initial claims in it. The processing power and memory capabilities of computers make them more efficient and less error prone for such reasoning tasks. 

Making decisions based on arguments, especially when the decision process and conditions are not explicitly programmed, is one area where humans are superior to computers at this moment. For example, when a human is asked `Is this a good film?' it can answer with a decision and often an explanation on how it came to that decision. You may not agree with it, but you may agree with parts of it or discover why you do not agree. Computers, for now, are limited to machine learning or statistical correlations for answering these vague questions, as there is not a strict definition or formula to define a good film.

But computers can learn, either from trying using approaches such as Machine Learning or statistical correlation, or by making use of the large amounts of knowledge in the form of argumentative text that humans have written. That way a computer cannot only learn what is a good film, but why it is a good film. And opposed to Machine Learning or statistical methods, you only have to let the computer encounter a single argument that says that humans find films made by Stanley Kubrick are good films because `he is an acclaimed director', and that films that are adapted from video games are bad films, because `that just never works'. And unlike with statistical approaches, the computer can now give an argued decision on why the next Tomb Raider film might not be a good film, without having seen tens of thousands of data points to train their classifier. But for this to work, computers need to be able to find, interpret and understand arguments.

Argument mining is the practise of extracting structured arguments from argumentative text, e.g. essays and dialogues, written by humans.  Often this is done by finding sentences that look argumentative based on keywords \citep{Marcu:1997kn}, and then constructing parts of the argument graph that form common argument structures (such as those described by \citet{Walton:2008vr}'s schemes) using a set of rules based on keywords or machine learning \citep{Mochales:2011eg}.
However, training such algorithms is difficult, and the complexity of natural language makes it challenging to interpret arguments completely. To evade this complexity modern argument mining techniques attack the three stages of argument mining---identification, segmentation and prediction---as classification challenges: a text is argumentative or not, a new claim starts here and ends there, and this claim supports that claim or not. These can be attacked using machine learning algorithms, which use keywords and other linguistic features as input, but do not have a strict understanding of the text. As a result, these approaches are forced to focus on finding hints of arguments in argumentative text instead of trying to gain a complete understanding, and, most importantly, exploring what such an understanding entails.

\paragraph{Our idea}
\begin{displayquote}
Very often natural language understanding entails the translation into another format that can be used by the machine to perform another task \hfill \citep{Moens:2018gd}
\end{displayquote}

\noindent We approach the argumentative text as a `natural' language that we transform into an argument structure using a parser and grammar. Specifically, we define a grammar for argumentative texts, and only text written in the language as defined by this grammar is to be interpreted. However, we want to keep this language as close to English as possible to make it easy to use and easy to interpret. We try two approaches: HASL/1 in which the grammar identifies every word and symbol in the text, and HASL/2 in which the grammar only describes the discourse markers (keywords such as `because' and `but') and symbols such as commas, and accepts the text between these without further interpretation.

Furthermore, we define an argument structure in terms of both grammar rules and a diagramming scheme. This argument structure combines the elements from \citet{Peldszus:2013jw}, such as \emph{independent} and \emph{cooperative} support and attack, with the concepts from Toulmin's argument model \citep{Toulmin:2003ec}, such as the concept of a \emph{warrant} that warrants a reason supporting a conclusion. It should also be able to express \citeauthor{Pollock:1987jx}'s \emph{undercutter}.

\input{diagrams/haslpipeline}

The grammar and the diagramming method will be two representations of the same underlying understanding of argumentation. The two representations are isomorphic: an argument that can be interpreted by the grammar can be drawn using the diagramming method, and a diagram can be formulated as an argumentative text. We can go from one to the other, and back, as drawn in \autoref{fig:haslpipeline}.

For the diagram representation to be as informative as the textual representation, we need to identify and resolve pronominal \emph{anaphora} (pronouns such as `he' and `these') because while in an argumentative text it is easy to find out where these refer to, in an argument diagram the order of the sentences that preceded claims with anaphora is lost, and anaphora resolution becomes difficult. Hence we should remove these pronouns from the text used in claims in the argument diagrams.

In this thesis we focus on these two representations to explore how well language can be mapped on a structured representation of arguments, but we also explore usage of this structure: we use the structured form of the argument to identify and fill in \emph{enthymemes}---arguments that have unstated premises or conclusions---and we use the argument diagrams to provide an alternative method of communicating the argument that is expressed in the argumentative text.

\paragraph{Requirements}\label{sec:requirements} To summarize, we define an argument structure, grammar, and implementation that allows us to accomplish the following requirements:

\begin{enumerate}
	\item Express pros: claims supporting a claim.
	\item Express cons: claims attacking a claim.
	\item Express an argument consisting of multiple supporting and attacking claims.
	\item Combine such expressions into a multiple sentences or a single, more complex sentence.
	\item Express cooperative and independent support and attack.
	\item Express warrants and undercutters.
	\item Express Toulmin arguments.
	\item Identify warrants.
	\item Interpret rule-like claims.
	\item Perform anaphora resolution.
	\item Perform enthymeme resolution.
	\item Formulate argumentative texts.
\end{enumerate}

\paragraph{Structure of this thesis}
We first discuss our argument structure in \autoref{sec:argumentation} using the diagram representation: each of the occurring patterns in this structure (i.e. particular types of support and attack relations) which we treat as our building blocks for argument structure. These can be combined to form any argument.

Next, for each of these elementary structures, we define one or more grammar rules. These dictate how the textual argument and diagram representation relate to each other. 

We then implement these building blocks and their grammars into a larger grammar which allows us to create argumentative texts of multiple sentences. This language, the human argument structure language, or HASL for short, is explored in two variants, each implementation exploring different aspects of the computational understanding of textual arguments.

\paragraph{HASL/1}
Our first experiment is HASL/1, which starts off with implementing the grammars for the building blocks: each sentence matches to one of these building blocks, and larger arguments can be constructed by writing multiple sentences, restating the claims that are relevant in each of them.

Our argument structure makes the distinction between claims that serve as direct support or attacking claims, and claims that serve as warrant. In HASL/1 we implement this as a distinction between general claims and specific claims. General claims are the more generalized rule-like claims such as `birds can fly', as opposed to specific claims, which talk about a very specific and particular claim, e.g. `Tweety can fly'. We make this distinction by using part-of-speech tags. As such, HASL/1 is a grammar built on top of a part-of-speech tagger, which is a common approach in natural language processing.

Using the structure we extract, we implement a form of enthymeme resolution. Enthymemes, the occurrence of premises that are part of an argument but are not explicitly stated, occur often in natural language \citep{Walton:2005dc,Reed:2004tb}. By filling in these missing premises in our argument graph we can make the right support or attack connections between claims, also when the targeted claim is not explicitly stated. We do this by treating building block around the attack and support relations as small syllogisms, and reconstruct the missing minor or major premise.

We also implement pronominal anaphora resolution. In HASL/1 claims that occur multiple times in argumentative text but that have the same meaning are merged into a single claim in our argument structure. In HASL/1 we determine whether two claims are the same by comparing their text. When this text contains references in the form of pronouns to other parts of the argument, for example if the premise is `he has wings', we need to check whether `he' refers to the same object. Otherwise claims that look the same but don't exactly mean the same are merged. Anaphora resolution helps us check this. It also helps with the understanding of arguments, as it makes the claims as drawn in the argument diagram more self-contained. One might say this is a necessary step since the information needed to resolve the anaphora as a human, namely the order of the sentences, is lost in the argument diagram.

\paragraph{HASL/2}
Because HASL/1 is built upon the part-of-speech tags of the words in the sentence, its grammar needs to define every combination of these that can occur in the claims. This part of the grammar is not relevant for the argument structure, and inhibits us from formulating syntactically novel claims.
In HASL/2 we explore a grammar that does not apply to the internal structure of the premises, but instead only looks at the discourse markers in the argumentative text: punctuation and keywords such as `because' and `except'.

This simplifies the grammar, focussing it on the argumentative structure instead of the structure of language in general. The remaining grammar is simple enough to make the implementation isomorph: HASL/2 is able to interpret textual arguments as argument diagrams, but can also formulate a textual argument when given an argument diagram. This makes HASL/2 useful to explore ambiguity in the language.

Furthermore, we found in HASL/1 that both the grammar rules that dictate how general claims are formulated, as well as the argument structure to represent them, to be incomplete: inside general claims there is a little rule structure on its own, dictating conditions and exceptions to their conclusions. For example, articles of law often have definitions, and general rules that describe when these definitions apply. Capturing this structure as well, we can understand argumentation both as it is applied (in an active discussion) and from where it originates (in the application of general rules), allowing for a deeper understanding that can help when evaluating an argument, or when reconstructing the missing claims in enthymemes.

\paragraph{Relevance}
HASL implements a form of argument mining, but one that is focussed on capturing all possible argumentative structures instead of capturing all possible formulations of such structures. This allows us to explore what a complete understanding of the argumentative text entails, such as the possibility to do enthymeme resolution. It also shows the possibilities and boundaries of interpreting argumentative text using only the information available in the text itself and given by the structure of our argument model.

The implementations of HASL presented here can be used to gain a better understanding of complicated arguments, where there are many premises that interact with each other. Anaphora and enthymeme resolution help with further understanding of the argument.

The language of HASL itself can be used for a new type of interface to the reasoning process of computer programs, allowing for the explanation of the reasoning behind a conclusion, or even as an interactive interface to engage in a dialogue of arguments with the computer.

\paragraph{Availability}
Both implementations of HASL can be found and experimented with online:

\begin{itemize}[label={}]
\item HASL/1: \url{http://hasl1.ikhoefgeen.nl/}
\item HASL/2: \url{http://hasl2.ikhoefgeen.nl/}
\end{itemize}
