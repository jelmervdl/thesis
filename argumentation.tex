\section{Argument structures}
\label{sec:argumentation}
The structure in arguments is made explicit by drawing it using boxes and arrows. These resulting diagrams, which we call argument diagrams, are used by HASL to represent the interpretation of the text. The structure also forms the basis on which the grammars of HASL are written.

This chapter explores that structure, describing how it should be interpreted and giving examples of what argumentative text could best be described by the structures. Our goal is to describe the most basic elements of argument structures which can be combined to form arguments. For each of the basic elements we will identify the general format of argumentative text which describes such an element.

\subsection{Claims and relations}
In our model, arguments consist of boxes, which represent claims, and arrows, which represent relations between those claims. A constellation of these form one large argument, where smaller parts of this constellation can be seen as local arguments that together contribute to the larger argument. For example, an argument about whether Jack should go to prison would encompass arguments about whether Jack committed a crime, and whether the crime is serious enough to require a prison sentence.

\paragraph{Claims} Claims themselves are drawn as a box. Claims are statements such as `Tweety can fly', `Socrates is a man', `Henry was born in Bermuda' but also `People born in Bermuda are generally British subjects'. Claims are the atoms of our argument. The conclusion is a claim, the statements supporting a conclusion are also claims. In previous research these boxes are drawn with only a reference (i.e. a letter or number) to the actual text \citep{Wigmore:1913pr,Thomas:1981,Peldszus:2013jw}. To make the argument diagram self-contained, we draw the actual text of the claim inside the claim box, as is done by \citet{Verheij:2005}.

\paragraph{Relations} Claims are then connected to each other by relations, drawn as arrows \citep{Wigmore:1913pr,Thomas:1981,Peldszus:2013jw,Verheij:2005}. To signal the position of the claim in the argument, these arrows either end with an arrowhead or a cross, indicating respectively a supporting or attacking relation \citep{Thomas:1981,Peldszus:2013jw,Verheij:2005}. Such a relation marks what is indicated in the argumentative text, i.e. what one of the arguing parties expressed to be a good support for the supported claim. For an arrow from $B$ to $A$ this can be as strong as `$A$ has to logically follow from $B$' or as assumptive as `I've seen many occurrences where $A$ results from $B$'. Our model does not depict whether the reason is a good reason for the conclusion or is true or false---a property that \citet{Verheij:2005} is able to express---only what was expressed in the argument itself.

\paragraph{Layout}
Like \citet{Verheij:2005}, but opposed to \citet{Peldszus:2013jw}, we prefer to draw the main claim of the argument at the top. We draw the graph, if possible, as a tree below it. If a relation connects to another relation instead of a claim, we prefer to draw it on the left or ride side of it. The algorithm for these layouts can be found in the implementation of HASL, and is also used for all the diagrams shown in this article.

\subsection{Support}
\label{sec:argsupport}
Claims supporting other claims, i.e. that describe the reason why the concluding claim must be the case, are drawn as a simple arrow between the two boxes that represent these claims. \autoref{fig:bbsupportex} shows examples these.

\input{diagrams/bbsupportex}

Often we call the box from which the arrow originates the \emph{supporting claim}, and the box to which the arrow points the \emph{supported claim} or \emph{conclusion}. In the context of a syllogism the supporting claim can also be interpreted as the \emph{premise}. In \citet{Toulmin:2003ec}'s model, it often matches the idea of a \emph{datum}.

The relation between the claims, i.e. the support relation specifically in these examples, determines the role of the claims. In \autoref{fig:bbsupportexchained} the claim that Socrates is human is supporting the claim that Socrates is mortal, and it is supported by the claim that his parents are human. As such the role of `Socrates is human' is both that of premise and conclusion.

\subsection{Cooperative and independent support}
\label{sec:cooperativeindependent}
A conclusion can also be supported by multiple supporting claims, but how these supporting claims support the conclusion can be different. Either \emph{independently}, by two separate claims or \emph{cooperatively}, by two claims that together form a single supporting argument \citep{Thomas:1981,Peldszus:2013jw}.

\input{diagrams/bbcombinations}

\paragraph{Independent support} When there are multiple but separate reasons to come to the same conclusion, the reasons are drawn with distinct support relations, as in \autoref{fig:bbindependent}. The idea is that if one of these reasons no longer holds, for example because it is successfully attacked, that this should have no impact on the other reasons. They are truly independent from each other, and each one of them is sufficient to on its own be a reason to conclude the conclusion.

\paragraph{Cooperative support} Some combinations of supporting claims can only support the conclusion when they occur and remain unchallenged together. For example, Dutch tort law dictates that someone has to repair damages only when four conditions are met. These four conditions then all need to be present, and all remain unchallenged, to support the conclusion that the tortfeasor has to repair the damages. The multiple relations coming together to form a single line in \autoref{fig:bbcooperative} symbolizes this.

\paragraph{Examples} In Dutch tort law \citep{Betlem:1993} we can find examples of both independent and cooperative support.

\begin{exe}
	\ex\label{exbbcooperative} John has a duty to repair the damages because someone suffered damages, those damages were caused by an act of John, the act was unlawful and the act can be imputed to John.
	\ex\label{exbbindependent} The act can be imputed to John because this article of law describes it as such and because common opinion describes it as such.
\end{exe}

\noindent In example~\ref{exbbcooperative} John has a duty to repair the damages because he meets all the conditions for this. If any of these conditions are not met, i.e. one of these claims is successfully attacked, the claim that John has a duty to repair damages is no longer supported.

In the second sentence, example~\ref{exbbindependent}, there are two reasons given for why the act can be imputed to John. Either one of them is sufficient, but in this example, for the sake of explaining, there are two given. Other examples could be eyewitness statements, where multiple witnesses claim to have seen something independently from each other. If the statement of one of these witnesses is challenged, the other statements remain unchallenged.

The combined argument from these two examples are drawn in \autoref{fig:bbcombinationsex}, in which the claims in example~\ref{exbbcooperative} are drawn as a connected single arrow, and those from example~\ref{exbbindependent} as two separate arrows.

\input{diagrams/bbcombinationsex}

\paragraph{Chained support} A third way of expressing multiple claims supporting a conclusion is through chained support, shown in \autoref{fig:bbchained}. This is essentially the combination of multiple arguments, as the conclusion of the second is the premise of the first. 

\begin{exe}
	\ex\label{exbbchained1} Tweety can fly because Tweety has wings. Tweety has wings because Tweety is a bird.
	\ex\label{exbbchained2} Tweety can fly because Tweety has wings because Tweety is a bird.
\end{exe}

\noindent The resulting semantics when evaluating the argument do not different from those of cooperative support: if any of the supporting claims is attacked, the topmost conclusion itself is no longer supported. The intention of chained support is different: Given examples~\ref{exbbchained1} or~\ref{exbbchained2} the question `Why can Tweety fly?' can now be answered with `Tweety has wings', and `Why does Tweety have wings?' with `Tweety is a bird'. We can continue this unendingly.

\subsection{Warranted support}
\label{sec:warrantedsupport}
Besides claims supporting conclusions, these relations between claims and conclusions can themselves also be supported. This should be interpreted as arguing on the argumentation itself: Supporting a support relation explains why that support relation should be taken serious, i.e. why the claim that Tweety is a bird is a valid reason to assume that Tweety can fly. A claim used in such a fashion is equal to the \emph{warrant} in Toulmin's model, or to the \emph{major premise} in a syllogism.

\input{diagrams/bbwarrant}

\paragraph{Warrants}
The warrant is a claim that supports the process of concluding the conclusion based on the presented premises. Both $A^s$ and $B^s$ in \autoref{fig:bbwarrant} can be unchallenged claims, but the process of $B^s$ supporting $A^s$ itself may also need to be supported. This is the role of the warrant, the relation between $C^g$ and the arrow pointing from $B^s$ to $A^s$.

\begin{exe}
	\ex\label{argwarrantex1} Tweety can fly because Tweety is a bird and birds can fly.
	\ex\label{argwarrantex2} Socrates is mortal because he is human and humans are mortal.
\end{exe}

\noindent Generally, $C^g$ in \autoref{fig:bbwarrant} is a general rule, e.g. `birds can fly' or `humans are mortal' in examples~\ref{argwarrantex1} and~\ref{argwarrantex2}. $B^s$ and $A^s$ are both specific to the situation and talk about individual instances, e.g. `Tweety is a bird' and `Tweety can fly'. We use the superscript $g$ and $s$ to indicate this. $C^g$ is the general case that, when applied to this specific situation discussed in the relation it supports, further explains why concluding that `Tweety can fly' makes sense given that `Tweety is a bird'. This becomes more apparent in \autoref{fig:bbwarrantex}, which shows the argument diagrams for examples~\ref{argwarrantex1} and~\ref{argwarrantex2}.

\input{diagrams/bbwarrantex}

\paragraph{General and specific claims}
The warrant should be interpreted as being a generalized rule, one without all the exceptions and conditions that are assumed to be irrelevant for the discussion at hand. As such, we will often refer to these claims as general claims.

We make a distinction between general claims and specific claims, for the reason that only general claims can serve as a warrant in an argument. A general claim is a statement that is more generally applicable than just the current argument, and which often is assumed to be undisputed among all participants of an argument. For example, `birds can fly' is a general statement, as opposed to `Tweety can fly'. However, `Tweety can fly' today could also be a general statement in the context of the more specific argument whether Tweety can fly today. As a rule of thumb, a warrant is a claim which does not change meaning in an argument when you put a qualifying keyword such as `generally' in front of it.

Note that the general claim is in itself still just a claim. It can be supported and attacked. For example, `birds can fly' itself can be supported by statistical claims that show that indeed most birds can fly. And such a support relation in turn can again be warranted by a claim stating that statistics is generally a sufficient method to show that such a rule of thumb may be stated.

\paragraph{Syllogism}
This construction of general claim, specific claim and conclusion takes the form of a syllogism. The general claim is the major premise, and the specific claims supporting the conclusion form the minor premise. The major premise warrants concluding the conclusion given the minor premise.

\paragraph{Conditional claim}
Often the warrant is also a conditional claim, and as such we can interpret the warrant as formal logic: `Birds can fly' can be interpreted as `something can fly \emph{if} something is a bird'. More generally: the warrant can be interpreted as a rule: `$X$ is a bird' is the condition for `$X$ can fly', where $X$ serves as a variable for some sort of reference to the individual to which the warrant is applicable. Each of the conditions should match up with a claim in the minor premise. If there are multiple conditions, they should be combined using cooperative support.

\subsection{Attack}
There are multiple types of attacks possible in an argument. We focus on claims themselves and on the context of a support relation, as these two elements essentially make up the building blocks of arguments. 

\input{diagrams/bbattack}

As \autoref{fig:bbattack} shows there are three strategies of attacking parts of the argument. One can attack any of the claims, or the relation between these two. In the context of this local argument, these three strategies are named rebutting, undercutting and undermining. 

\input{diagrams/bbattackex}

\paragraph{Counter-claims} The example in \autoref{fig:bbattackexexplicit} is an example of such an attack which just claims the opposite. These two claims are clearly incompatible with each other, and as such exclude each other. If one of them is indeed true, the other has to be false (baring the context of time in this example). As such, we can draw the attack relation between these two claims as a mutual attack relation.

The second example, \autoref{fig:bbattackeximplicit} illustrates that the attack relation between two claims is not always as easy to perceive as in the first example. Here, only after knowing the meaning of being a bird or squirrel it is that one can conclude that being a bird excludes being a squirrel.

\paragraph{Counter-examples} In the case of general claims, which are often generalized rules, these attacking claims can take the form of a counter-example, which is what happens in the example in \autoref{fig:bbattackexcounterexample}. This can only be concluded after multiple inference steps, namely that `All swans' implies that any swan seen by me must also be black, and that being black excludes being white. Again, these two claims mutually exclude each other.

\input{diagrams/bbattackex2}

\paragraph{Rebuttal} Rebuttal in an argument is finding a way of directly attacking its conclusion, for example by claiming the opposite of the conclusion, or by showing that it is impossible or incorrect. The claim that Tweety cannot fly in the example in \autoref{fig:bbrebutterex} is a rebutter.

\paragraph{Undercutter} The supporting relation between claim and conclusion may be warranted, but it might not be (or no longer be) relevant when evidence comes to the table that the situation discussed is an exception to the rule. This exception, named an \emph{undercutting defeater} by \citet{Pollock:1987jx}, then takes the role of attacking this support relation. An example of such an undercutter indicating an exception: 

In \autoref{fig:bbundercutter} the general form of an undercutter is drawn. Note that the undercutter does not attack the warranting claim itself. In the example in \autoref{fig:bbundercutterex} it does not counter the claim that Tweety is a bird, nor that Tweety can fly. However, in the light of the undercutting claim that not all birds can fly, the claim that Tweety is a bird no longer lets us say anything about whether Tweety can fly.

\paragraph{Underminer} Lastly, the supporting claim itself can be attacked, shown in the example in \autoref{fig:bbunderminerex}. Here, the support for the claim that Tweety can fly is questioned, but the conclusion itself is not. If the argument continues it could be that a new claim supporting the conclusion is presented, e.g. that Tweety is a flying squirrel, or the attack is continued in the form of the warrant that squirrels cannot fly and hence Tweety cannot fly, finally forming a rebuttal.

\paragraph{Cooperative and independent attacks} Attacks can attack the same claim in the same fashion as supporting claims can, both cooperatively and independently.

% How about warranted attacks? To me, they don't make much sense. It would be easier to make the attacking argument a conclusion that itself is supported by a warranted support.

\paragraph{Attack in warranted support}
If we also warrant the support relation by introducing a general claim that supports the support relation itself, we have two more opportunities for attacks: the newly added arrow, and the general claim. The first we treat the same as undercutting, and the second as rebuttal of the general claim.

\paragraph{Attack versus mutual attack}
In our argument diagrams we do not draw them as mutual attack for the reason that in the argument itself, a claim is often presented as an attack on another claim. As such, the attack clearly has a direction after the intention of the participant of the argument that brought the claim to the argument. We would like to capture that direction and intention. In argumentative text this intention is presented in the word order: in `$A$ but $B$' it is $B$ that attacks $A$.

This should not pose any problems, as it is up to the producer of the arguments to correctly state the intentions and ignoring those by filling in a mutual attack would do more harm. It is also not important for the correctness of the argument as our goal is to capture the intention of the argument as it is presented, not as it should be by all theories of correct argumentation.

For the evaluation of arguments it does not matter whether the attack relation is bidirectional or not: either the attacked claim holds, and as such the attacking claim cannot hold, or the attacking claim holds, and thus the attacked claim does not.

The rebuttal of an argument is essentially equal to attacking it by stating (and preferably supporting) the opposite of its conclusion. In that light, a rebuttal could also be drawn as a claim stating the opposite of the current conclusion, an attack relation between these two, and a support relation originating from your rebutting claim to the newly added counter-conclusion.

\subsection{Rules \& reasons}
\label{sec:explicitrulesandconditions}
Argumentation and rule-like text of general claims are closely related: the structure where claims are evidence for a conclusion works both for arguments as well as for rules. We do treat them differently though, as arguments describe the currently discussed situation whereas rules describe the general case. This is why in \autoref{fig:wbbintro} we draw them using the same arrows, but limit the condition and exception relations to the box of the general claim in the argument.

\begin{figure}
	\centering
	\begin{haslpicture}[scale=0.5]{wbbintro}
		a: Tweety can fly
		b: He is a bird
		c: Something can fly
		d: It is a bird
		e: b supports a
		f: d warrants c
		g: c supports e
	\end{haslpicture}
	\haslsentence{Tweety can fly because he is a bird and something can fly if it is a bird.}
	\caption{The warrant is analysed further, and its conditions are made explicit. The structure of the argument itself is expected to match the structure of the rule-like warrant.}
	\label{fig:wbbintro}
\end{figure}

In this subsection we further explore this rule structure, which follows the same lines as the general structure for argumentation, including cooperative and independent conditions, and exceptions that are modelled as undercutters.

\paragraph{Defeasible rules} The warrant in argumentation is often not complete: it only describes the general case, for example `birds can fly', but does not explicitly mention all the exact conditions or all possible exceptions, i.e. birds that have wings that can carry their weight can fly, or that birds except penguins and ostriches and a few other exceptions can fly. In this manner, only the general rule, i.e. `generally birds can fly', is expressed, and if there is an exception to this rule applicable in the current argument, it is expressed as a separate statement. Still, even a defeasible rule has conditions that have to be met for it to be in any way applicable.

\paragraph{Explicit conditions} The following example demonstrates the transition between argumentative text as we have discussed it until now, and rules which are formulated as applied logical expressions, to show these conditions:

\begin{exe}
	\ex\label{di1} Tweety can fly because Tweety is a bird and birds can fly.
	\ex\label{di15} Tweety can fly because Tweety is a bird and something can fly if it is a bird.
	\ex\label{di2} Tweety can fly because Tweety is a bird and Tweety can fly if Tweety is a bird.
\end{exe}

\noindent The first example,~\ref{di1}, is the familiar example where `birds can fly' is the general claim and `Tweety is a bird' is the specific claim, and the general claim warrants the specific claim's support for the conclusion that Tweety can fly.\\
Example~\ref{di15} and~\ref{di2} are similar sentences, except that the general claim is being reformulated in a more rule-like formulate and applied to Tweety: `Tweety can fly if Tweety is a bird'.

The general claim `birds can fly' in example~\ref{di1} hides the conditions that are explicit in example~\ref{di2}, i.e. something needs to be a bird if it is capable of flight according to this rule, it cannot be applied to dogs for example.

In our argument structure we expressed exceptions to general claims as rebutters if they directly attack the conclusion (i.e. claim the opposite, like `Tweety cannot fly') or as undercutters if they cause the support to become irrelevant (i.e. the undercutting claim causes the support relation to longer be evidence for the conclusion, for example by expressing that the specific case being discussed is an exception to the general rule that is used as warrant).

The conditions that are hidden in example~\ref{di1} and made explicit in example~\ref{di2} are not yet drawn in our argument diagram as these themselves are not part of the argumentation process: a general claim is called upon during argumentation, but not actively formed in that same argument, hence the individual conditions are not explicitly discussed.

\paragraph{Reasons for drawing conditions} Still, we would like to be able to express these conditions in a more systematic way as they are relevant to arguing: when following along with an argument, one needs to check whether these conditions are met by the instance mentioned in the specific claims, and whether the conclusion of the general claim is then also the conclusion of the argument after being applied to the specific subject discussed. If not, then either the specific claims are incomplete or incorrect, or the general claim is not the right warrant for this argument.

For HASL there are two reasons to identify the conditions in rules explicitly: first it can help with enthymeme resolution because when given the conditions of the general claim and the conclusion, the specific claims can be filled in. Second, general claims might become rather complex. Take for example the argument shown in \autoref{fig:bbcombinationsex}: the warrant for concluding whether someone needs to repair the damages in a tort case states all kinds of conditions that the person and the scenario need to meet before being applicable. Additionally, tort law also explicitly states a number of exceptions, circumstances in which all the conditions might be met but in which the conclusion is not applicable. In such arguments, these exceptions might be explicitly stated when stating the complete warrant by for example citing an article from tort law, but the exception might not be used in the argument itself. We want to have a diagramming structure to express this construction of conditions and exceptions which occurs inside the general claim.

\subsubsection{Structure inside general claims}
\label{sec:rules}
\autoref{fig:wbb} shows the building blocks which can occur inside a general claim. These are modelled after the support and attack relations for arguments themselves.

\input{diagrams/wbb}

\paragraph{conditions} conditions as shown in Figure~\ref{fig:rbc1}, \ref{fig:rbc2r} and~\ref{fig:rbc2u} are conditions that need to be met for the conclusion of the rule-like claim to be applicable. They can be cooperative or independent to each other, similar to how claims can cooperatively and independently support other claims (\autoref{fig:bbcombinations}).

\input{diagrams/wbbcombinations}

How these conditions are described in argumentative text varies greatly. As shown in the introduction of this section it could be as explicit as `if something is a bird it can fly' or as casual as `birds can fly'.

The process of extracting the conditions from formulations such as `birds can fly' is not described in detail here as it in itself is a task as discourse analysis itself. Therefore the following grammars are but a subset of all possible ways to express conditions inside general claims.

\input{diagrams/wbbcombinationsex}

\autoref{fig:wbbcombinationsex} shows examples of arguments that can be described using structures \autoref{fig:wbbcombinations}.

\paragraph{Exceptions}
Each condition can have its own exception, similar to how undercutters can form exceptions to general claims (\autoref{fig:rbe1}):

\begin{exe}
	\ex\label{argruleex6} If it is a bird it can fly except if it is a penguin.
	\ex\label{argruleex7} Birds can fly except when they are penguins.
	\ex\label{argruleex8} Birds except penguins can fly.
\end{exe}

\noindent There again can be linked (\autoref{fig:rbe2r}) and unrelated (\autoref{fig:rbe2u}) conditions for these exceptions. Examples of general claims with one condition which have one exception that has multiple conditions, drawn in \autoref{fig:argruleex9to10}:

\begin{exe}
	\ex\label{argruleex9} If it is a bird it can fly except if it has wings and they are clipped.
	\ex\label{argruleex10} Birds can fly except when they have clipped wings.
\end{exe}

\noindent Examples of general claims with one condition that have multiple exceptions, drawn in \autoref{fig:argruleex11to13}:

\begin{exe}
	\ex\label{argruleex11} If it is a bird it can fly except if it is a penguin or if it is an ostrich.
	\ex\label{argruleex12} Birds can fly except when they are penguins or they are ostriches.
	\ex\label{argruleex13} Birds except penguins and ostriches can fly.
\end{exe}

\begin{figure}[h]
	\centering
	\begin{subfigure}[t]{0.3\textwidth}
		\centering
		\begin{haslpicture}[scale=0.5]{argruleex6to8}
			w: It can fly
			c: It is a bird
			e: It is a penguin
			a: c warrants w
			b: e undercuts a
		\end{haslpicture}
		\haslsentence{Birds except penguins can fly.}
		\caption{Example of a general claim with one exception.}
		\label{fig:argruleex6to8}
	\end{subfigure}\hfill
	\begin{subfigure}[t]{0.3\textwidth}
		\centering
		\begin{haslpicture}[scale=0.5]{argruleex11to13}
			w: It can fly
			c: It is a bird
			e1: It is an ostrich
			e2: It is a penguin
			a: c warrants w
			b: e1 undercuts a
			c: e2 undercuts a
		\end{haslpicture}
		\haslsentence{Birds except penguins and ostriches can fly.}
		\caption{Example of a general claim with multiple exceptions.}
		\label{fig:argruleex11to13}
	\end{subfigure}\hfill
	\begin{subfigure}[t]{0.3\textwidth}
		\centering
		\begin{haslpicture}[scale=0.5]{argruleex9to10}
			w: It can fly
			c: It is a bird
			e1: It has wings
			e2: The wings are clipped
			a: c warrants w
			b: e1 e2 undercuts a
		\end{haslpicture}
		\haslsentence{Birds can fly except when they have clipped wings.}
		\caption{Example of a general claim with one exception that has multiple conditions.}
		\label{fig:argruleex9to10}
	\end{subfigure}
	\caption{Examples of how we can have multiple exceptions or a single exception with multiple conditions.}
\end{figure}

\input{diagrams/argruleexcombined}

\paragraph{Combined example} Using the abstract argumentation structure described in \autoref{fig:bbcombinations} and \autoref{fig:wbb} we can combine arguments like shown in  \autoref{fig:argruleexcombined}, in which specific claims are supported by a general claim, and the general claim is interpreted as a rule-like claim with conditions and exceptions. The specific claim is indeed supported by a claim that matches the condition of the general claim, and none of the exceptions are present in the argument.

% It is less likely that such complex general claims are used in natural textual argumentation unless required for the argument since communicating the minimal amount of information necessary to understand the argument is the optimum for efficient and convincing argumentation.
