\subsubsection{Anaphora}
We evaluate anaphora resolution by replacing some of the names from the previous examples to pronouns. For example, we take sentence~\ref{evalsylsoc} and replace `Socrates' with `he' to create sentence~\ref{evalanasoc1}. We then also replace `men' with `they' to create~\ref{evalanasoc2}.

\begin{exe}
    \ex\label{evalanasoc1} Socrates is mortal because he is a man and men are mortal.
    \ex\label{evalanasoc2} Socrates is mortal because he is a man and they are mortal.
\end{exe}

Anaphora resolution is necessary to determine whether a repeated claim that is textually the same (i.e. `he has eaten spoiled food' in sentence~\ref{evalanadouble2}) has the same meaning, or if it is in fact a different claim because `he' refers to a different person.

\begin{exe}
    \ex\label{evalanadouble0} John is ill because he has eaten spoiled food. John is ill because Henry is ill.

    \ex\label{evalanadouble1} John is ill because he has eaten spoiled food and because Henry is ill.

    \ex\label{evalanadouble2} John is ill because he has eaten spoiled food and because Henry is ill because he has eaten spoiled food.
\end{exe}

\subsubsection{Tort}
This evaluation is based on the translation of \citet{Betlem:1993} of Dutch Tort law: Article 6:162 Definition of a `tortious act' (unlawful act).

\begin{exe}
    \ex\label{evaltortorig} A person who commits a tortious act against another person that can be attributed to him, must repair the damage that this other person has suffered as a result thereof.\\
    As a tortious act is regarded a violation of someone else's right, an act or omission in violation of a duty imposed by law or of what according to unwritten law has to be regarded as proper social conduct, always as far as there was no justification for this behaviour.\\
    A tortious act can be attributed to the tortfeasor if it results from his fault or from a cause for which he is accountable by virtue of law or generally accepted principles.
\end{exe}

\noindent In this original translation the keywords `and' and `or' we use to separate claims are also used inside claims themselves. This may cause trouble, which is why we also try a small adaptation that replaces the use of `or' in such cases with a slash. We also make the individual claims (i.e. the conditions) more distinct by adding comma's. We also change the main claim of every sentence to match the exact wording of the condition in one of the previous sentences. Since HASL/2 does not try to comprehend the claims themselves.

\begin{exe}
    \ex\label{evaltortstrict} A person must repair the damage if he committed an act against another person, the act is tortious, the act can be attributed to him, and the other person has suffered the damage as a result thereof.\\
    The act is tortious if there was a violation of someone else's right, if an act/omission is in violation of a duty imposed by law, or if an act/omission is in violation of what according to unwritten law has to be regarded as proper social conduct unless there was a justification for this behaviour.\\
    The act can be attributed to him if it results from his fault or if it results from a cause for which he is accountable by virtue of law/generally accepted principles.
\end{exe}

\noindent \todoinline{This text reads is in a passive form, much like the categorial syllogism. HASL/0 and HASL/1 cannot handle those kinds of texts. For this reason we rewrite the text into a more active form where there is always a single claim supported by one or more claims, which themselves can be supported or attacked by other claims.}

\begin{exe}
    \ex\label{evaltortdirect} The person must repair the damages because
        he has committed an act,
        he can be attributed the act,
        the act is tortious,
        it is an act against another person
        and the other person has suffered the damages of it.\\
    The act is tortious
        because it is a violation of a right,
        because it is a violation of a law,
        because it is a violation of an unwritten law
        and because it is an omission
        except it is justifiable.\\
    The person can be attributed the act because he is accountable and because it is the fault of the person.
\end{exe}

\noindent HASL/0's grammar does not allow for complex constructions of claims in a single sentence, which is why we also test a more simple form of the same sentence.

\begin{exe}
    \ex\label{evaltortsimple} The person must repair the damages because
    the person has committed an act,
    the person can be attributed the act,
    the act is tortious,
    the act is an act against another person
    and the other person has suffered the damages of the act.\\
    The act is tortious because the act is a violation of a right except the act is justifiable.\\
    The act is tortious because the act is a violation of a law except the act is justifiable.\\
    The act is tortious because the act is a violation of an unwritten law except the act is justifiable.\\
    The act is tortious because the act is an omission except the act is justifiable.\\
    The person can be attributed the act because the person is accountable.\\
    The person can be attributed the act because the act is the fault of the person.
\end{exe}

% \input{evaluation/eyewitness}

\subsubsection{Toulmin}
We compare HASL to Toulmin's using his examples. We show how HASL could handle the textual form of Toulmin's arguments.

\paragraph{Toulmin's model} All elements, except the quantifier, of Toulmin's model \citep{Toulmin:2003ec} can be expressed in the argument diagram. This is shown in \autoref{fig:bbtoulmin}. In this diagram all elements have the same semantic meaning as they have in Toulmin's model.
The rebuttal can be placed elsewhere as well, as Toulmin does not distinguish attack relations on what element they attack while argument diagrams do: the rebuttal in \autoref{fig:bbtoulmin} could also have been attacking the claim, datum or warrant, all of which can be expressed using the building blocks from \autoref{fig:bb}, or to the support relation between the claim and datum as an undercutter (\autoref{fig:bbundercuttingdefeater}).

\begin{figure}[h]
    \centering
    \begin{subfigure}[t]{0.45\textwidth}
        \centering
        \begin{haslpicture}[scale=0.5]{bbtoulmin}
            d: Datum
            b: Backing
            w: Warrant
            c: Claim
            r: Rebuttal
            rd: d supports c
            rw: w supports rd
            rb: b supports w
            rr: r attacks rw
        \end{haslpicture}
        \caption{Terms of Toulmin's model expressed in proposed argument structure.}
    \end{subfigure}
    \begin{subfigure}[t]{0.45\textwidth}
        \begin{haslpicture}[scale=0.25]{toulminexample}
            c: Harry is a British subject
            d: Harry was born in Bermuda
            w: A man born in Bermuda will be a British subject
            u2: He has become a naturalised American
            b: The following statutes and other legal provisions
            1: d supports c
            2: w supports 1
            3: b supports w
            5: u2 attacks c
        \end{haslpicture}
        \caption{Example argument described by Toulmin in this structure.}
    \end{subfigure}
    \caption{Toulmin's model expressed in the argument structure expressed in this article.}
    \label{fig:bbtoulmin}
\end{figure}


% Well, we don't have an answer to the qualifiers Toulmin defines as necessary. We just don't have any of those... well maybe you might add them yourself to the claims. They don't affect the process of attacking and supporting claims I think, so they can be left out or added when appropriate.

% I also don't make the distinction between force and criteria as he does. Again, I don't think it affects the structure of the argument, only the evaluation (or validation for that matter)

% Harry's hair is not black because Harry's hair is red and if anything is red, it will not be black.

%%% Vergelijken met Toulmin

When we compare HASL's ideal of the warrant to Toulmin's, we see HASL's is less well defined. An an example, Toulmin gives the syllogism in example~\ref{evaltoulanne}.

We rewrite this syllogism also to a more natural form in example~\ref{evaltoulannenat}. The first thing to notice is that, since HASL does not have any grammars for qualifiers, the `all' from the universal claim is dropped in translation. \todoinline{Toulmin would find this horrific, as he, not unsuccessfully, argues that these qualifiers are very small but strong hints at the backing that is not mentioned. All makes this an almost analytical argument, `some' would make it more statistical, `most' more like an argument based on a gut feeling.}


\begin{exe}
    \ex\label{evaltoulanne} Anne is Jack’s sister;\\
        All Jack’s sisters have red hair;\\
        So Anne has red hair.
    \ex\label{evaltoulannenat} Anne has red hair because Anne is Jack's sister and Jack's sisters have red hair.
\end{exe}

\input{evaluation/table}

\subsection{HASL/0}

\subsubsection{Syllogisms}
HASL/0 correctly interprets the syllogisms in sentences \ref{evalsylsoc} and \ref{evalsyljohn} (Figures~\ref{fig:h0evalsylsoc} and~\ref{fig:h0evalsyljohn}).

\subsubsection{Enthymemes}
HASL/0 can interpret an enthymeme where the major claim is missing (sentence~\ref{evalenthsocman}, \autoref{fig:h0evalenthsocman}). If it is the minor claim or the conclusion that is missing (sentences~\ref{evalenthsocmortal} and~\ref{evalenthmanmortal}) they cannot be interpreted as the grammar does not allow for a singular major claim and always expects a discourse marker (the `because').

\subsubsection{Categorial syllogisms}
HASL/0 cannot parse the categorial syllogisms in sentences \ref{evalcatsylmen} and \ref{evalcatsylcar}. As HASL uses the distinction between singular and general to identify the claim and the warrant, categorical syllogisms pose a difficulty. Even though `a car' in the second example is singular, it is interpreted as a general claim since it does not designate a single particular car.

\subsubsection{Anaphora}
The anaphora in sentences~\ref{evalanasoc1} and \ref{evalanadouble0} are interpreted as intended (Figures~\ref{fig:h0evalanasoc1} and~\ref{fig:h0evalanadouble0}), but the anaphora (`he' in both sentences) are not recognized.

In sentence~\ref{evalanasoc2} (\autoref{fig:h0evalanasoc2}) the major claim is not recognized as such, and the interpretation is not as intended.

Sentences~\ref{evalanadouble1} and~\ref{evalanadouble2} cannot be parsed by HASL/0 because of the nested arguments.

\subsubsection{Tort}
HASL/0's grammar cannot interpret the argument in example~\ref{evaltortorig}. Additionally, the conclusion (or antecedent if we would interpret the sentence as a rule) has to be singular, as previously observed in the evaluation of the categorical syllogism.

For HASL/0 to be able to interpret the argument, the argument has to be formulated as a monologue in favour of the conclusion. Each claim has to be stated on its own and has to start or reiterate the conclusion. Only the discourse marker `because' can be used, `when' or `if' are not supported. This formulation, stated in~\ref{evaltortsimple}, is correctly interpreted by HASL/0 in \autoref{fig:h0evaltortsimple}.

\subsection{HASL/1}

\subsubsection{Syllogisms}
The interpretations sentences \ref{evalsylsoc} and \ref{evalsyljohn} of by HASL/1 (Figures~\ref{fig:h1evalsylsoc} and~\ref{fig:h1evalsyljohn}) are exactly the same as those of HASL/0. The grammar differs, but basic arguments are interpreted the same.


\subsubsection{Enthymemes}
The major and minor claim in sentences~\ref{evalenthsocman} and~\ref{evalenthsocmortal} are correctly identified and filled in in \autoref{fig:h1evalenthsocman} and \autoref{fig:h1evalenthsocmortal}. The missing conclusion in sentence~\ref{evalenthmanmortal} cannot be filled in since the grammar cannot parse sentences without a conclusion.

\subsubsection{Categorial syllogims} HASL/1 has the same problem as HASL/0 that it assumes that when two or more claims form a supporting claim (`because men are mammals and mammals are mortal') that only one of them is general: the warrant. It cannot parse examples~\ref{evalcatsylmen} and ~\ref{evalcatsylcar}.

\subsubsection{Anaphora}
HASl/1 is able to parse all example sentences~\ref{evalanasoc1} to~\ref{evalanadouble2}.

In sentence~\ref{evalanasoc1} (\autoref{fig:h1evalanasoc1}) `he' is correctly identified as Socrates.

In sentence~\ref{evalanasoc2} (\autoref{fig:h1evalanasoc2}) the `they' which should refer to the unstated `men' in `men are mortal' is identified as a pronoun, but not linked to any other instance or noun. Initially the claim `they are mortal' is not recognized as the major claim in this syllogism as HASL is not able to determine whether `they' refers to something that identifies a category of things (i.e. `men') or a group of individuals (i.e. `these man'). This distinction is used to differentiate minor from major claims. Because the major claim is not identified as such, the argument is treated as an enthymeme and the major claim is filled in by HASL itself. The resulting major claim however inherits the number (in this case singular) of the subject of the minor claim. Its subject `a man' can therefore not be connected to `they' afterwards as `they' is identified as plural.

\todo{So an improvement would be to always assume plural for the filled in major claims. Because that would not be a wild assumption.}

Sentences~\ref{evalanadouble0} and~\ref{evalanadouble1} are interpreted correctly (Figures~\ref{fig:h1evalanadouble0} and~\ref{fig:h1evalanadouble1}). In both cases `he' is correctly connected to `John'.

Sentence~\ref{evalanadouble2} (\autoref{fig:h1evalanadouble2}) only differs from sentence~\ref{evalanadouble1} in that the claim `Henry is ill' has its own supporting argumentation. This additional supporting claim, which as a pronoun `he' that should refer to `Henry', causes the `he' in the claim that supports John being ill to also be interpreted as referring to `Henry'. Presumably this happens because `he' is now found to already be referring to `Henry', and the second `he' that is found while constructing the argument (note that the argument is constructed from the sentence approximately from right to left) matches with the previously encountered and already resolved `he'.


\subsubsection{Tort}
The examples~\ref{evaltortorig} and~\ref{evaltortstrict} cannot be parsed by HASL/1's grammar.

HASL/1 interprets example~\ref{evaltortdirect} in two different argument diagrams (Figures~\ref{fig:h1evaltortdirect1} and~\ref{fig:h1evaltortdirect2}). The argument is interpreted as ambiguous because the second sentence, `The act is tortious because ... and because it is an omission except it is justifiable.' is interpreted ambiguously: the exemption can apply either to the main claim `the act is tortious' or the last supporting argument `it is an omission'.

In example \autoref{fig:h1evaltortdirect} (\autoref{fig:h1evaltortdirect}) HASL/1 finds many of the arguments to be enthymemes and has filled in the following warrants:

\begin{enumerate}
    \item Something can be attributed the act if something is the fault of the person
    \item Something can be attributed the act if something is accountable
    \item Something is not tortious if something is justifiable
    \item An omission is tortious
    \item A violation of an unwritten law is tortious
    \item A violation of a law is tortious
    \item A violation of a right is tortious
    \item Something must repair the damages if something has committed an act, something can be attributed the act and something is against another person
\end{enumerate}

\noindent HASL/1 assumes warrants for every arrow individually, resulting in eight smaller rules instead of the three we gave it.

HASL/1 is also able to interpret example~\ref{evaltortsimple}, but finds it highly ambiguous as it finds 256 interpretations. The ambiguity is caused by grammar rules that allow some parts of claims to be interpreted through different rules, but the resulting argument diagram is the same. \autoref{fig:h1evaltortsimple} shows the first of these interpretations.

\subsubsection{Toulmin}

\paragraph{Toulmin}
Our argument schema can emulate most elements of Toulmin's model. The datum, warrant and conclusion directly map to our warranted support structure. We do not have a specific element for the qualifier of an argument, and our attacks are specifically attacking a claim in the argument as where Toulmin gives attacking arguments their own place in the argument itself.

We formulated two of Toulmin's example arguments, Examples~\ref{ex:harry} and~\ref{ex:petersen}. In the first example we intentionally left out the warrant to test whether this is picked up by HASL. In the second example we do state the warrant (`a Swede can be taken...'), and support it with a backing (`because the portion of...').

\begin{exe}
    \ex\label{ex:harry} Harry is a British subject because Harry is a man born in Bermuda but Harry has become naturalized.
    \ex\label{ex:petersen} Petersen will not be a Roman Catholic because he is a Swede and a Swede can be taken almost certainly not to be a Roman Catholic because the proportion of Roman Catholic Swedes is less than 2\%.
\end{exe}

\noindent Example~\ref{ex:harry} shows the same ambiguity in interpretation we saw in \autoref{fig:hasl1harry}: the are three ways to interpret the attack. In two of the three interpretations the warrant is identified, but in the first interpretation it is missing for the reasons described earlier with \autoref{fig:evalcons2a}.

\input{diagrams/hasl1harry}

In \autoref{fig:hasl1petersen} the interpretations of Example~\ref{ex:petersen} are shown. We have drawn both the interpretations without and with enthymeme resolution to highlight the effect of adding unstated claims to the argument diagram. It shows that although qualifiers are not intentionally taken into account, their meaning is picked up and reflected in the argument.

Furthermore the backing is always correctly placed as supporting the warrant.

\input{diagrams/hasl1petersen}

\subsection{HASL/2}

\subsubsection{Syllogism} HASL/2 has two interpretations of sentences \ref{evalsylsoc} and \ref{evalsyljohn} (Figures~\ref{fig:h2evalsylsoc} and \ref{fig:h2evalsyljohn}) as it does not make the distinction between general and singular claims: the second claim can be both interpreted as a warrant and as a second datum.

\subsubsection{Enthymemes}
HASL/2 is able to interpret the enthymeme in sentence~\ref{evalenthsocman} (\autoref{fig:h2evalenthsocman}) correctly, but does not fill in the missing major claim. HASL/2 does not parse the claims themselves, cannot select the subject or object of claims and therefore is not able to construct a major claim like HASL/1.

The interpretation of sentence~\ref{evalenthsocmortal} (\autoref{fig:h2evalenthsocmortal}) is discussable. The major claim is interpreted as a supporting claim, but not as a warrant. For it to be interpreted as a warrant, a minor claim, or at least the position of the minor claim has to be present, otherwise there is no arrow between the conclusion and the minor claim which the major could support. HASL/2 cannot fill in a minor claim in this case, hence the major claim is interpreted as just a claim supporting the conclusion.

The sentence~\ref{evalenthmanmortal} cannot be interpreted by HASL/2 for the same reason as HASL/0 and HASL/1: the grammar assumes there is always a conclusion to be supported.

\subsubsection{Categorial syllogism} HASL/2 does not make the distinction between singular and general claims, and can interpret the examples \ref{evalcatsylmen} and \ref{evalcatsylcar} (Figures~\ref{fig:h2evalcatsylmen} and \ref{fig:h2evalcatsylcar}). However, just as in the syllogism example, there are two interpretations of both examples: one in which the most general claim is the warrant and one in which both supporting claims are treated the same.

\subsubsection{Anaphora}
HASL/2 is not able to do pronominal anaphora resolution because it does not interpret the claims themselves. It has no knowledge of the presence of names, nouns or pronouns.

Sentences~\ref{evalanasoc1} and~\ref{evalanasoc2} (Figures~\ref{fig:h2evalanasoc1} and~\ref{fig:h2evalanasoc2}) are interpreted in two ways, like with the categorial syllogisms: HASL/2 cannot distinguish between singular and general claims, hence the second supporting claim is always interpreted as either of these once. The use of anaphora have no effect on the interpretation.

Example~\ref{evalanadouble0} has only one supporting claim per sentence, which causes them to be always interpreted as minor claims (\autoref{fig:h2evalanadouble0}). The two sentences are then merged into a single argument.

Example~\ref{evalanadouble1} (\autoref{fig:h2evalanadouble1}) is interpreted correctly.

In the interpretation of example~\ref{evalanadouble2} (\autoref{fig:h2evalanadouble2}) the claim `he has eaten spoiled food' is textually repeated but each repetitions supports a different claim since the first time `he' refers to John, the second time it refers to Henry. Since HASL/2 is not able to detect that `he' causes the claim to not be exactly the same claim, it is drawn only once in the argument diagram. This interpretation is incorrect.

\subsubsection{Tort}
HASL/2 is only able to interpret example~\ref{evaltortstrict} due to its grammar rules. The interpretation (\autoref{fig:h2evaltortstrict}) is almost correct, except for the `justification for this behaviour'. Here this is interpreted as an undercutter, but ideally it would have been an undercutter for all three relations supporting the `the act is tortious' claim, or as an rebuttal of that claim. \todoinline{This last interpretation is not possible with the grammar of HASL/2 at this moment.}

\subsubsection{Diagram to text}
HASL/2 can create textual arguments from diagrams. For the diagram in \autoref{fig:h2evaltortstrict} this yields three different formulations. The differences are highlighted.

The first formulated text is the argument interpreted as a direct argument. This happens because HASL/2 is not able to distinct singular claims from general claims as it does not look at the words inside claims. The result is difficult to read, especially due to the supporting argument (emphasized) for `the act is tortious' occurring directly inside another supporting argument (an argument supporting `a person must repair damage').

\begin{quotation}
A person must repair the damage \textbf{because} he committed an act against another person, the act is tortious \textbf{because} there was a violation of someone else's right, \emph{because an act/omission is in violation of a duty imposed by law, \textbf{and because} an act/omission is in violation of what according to unwritten law has to be regarded as proper social conduct \textbf{except} there was a justification for this behaviour}, the act can be attributed to him \textbf{because} it results from his fault and because it results from a cause for which he is accountable by virtue of law/generally accepted principles, and the other person has suffered the damage as a result thereof.
\end{quotation}

The second and third formulation both interpreted the diagram as a general claim with conditions and exceptions, i.e. as a warrant to be used in an argument. The resulting language fits better, and the first formulation is the exact input we gave it when generating the diagram.

The third formulation only differs from the second in that it uses `except if' instead of `unless' to mark the undercutter.

\begin{quotation}
A person must repair the damage \textbf{if} he committed an act against another person, the act is tortious, the act can be attributed to him, and the other person has suffered the damage as a result thereof.\\
The act is tortious \textbf{if} there was a violation of someone else's right, \textbf{if} an act/omission is in violation of a duty imposed by law, \textbf{or if} an act/omission is in violation of what according to unwritten law has to be regarded as proper social conduct \textbf{unless} there was a justification for this behaviour.\\
The act can be attributed to him \textbf{if} it results from his fault \textbf{or if} it results from a cause for which he is accountable by virtue of law/generally accepted principles.
\end{quotation}

\begin{quotation}
A person must repair the damage \textbf{if} he committed an act against another person, the act is tortious, the act can be attributed to him, and the other person has suffered the damage as a result thereof.\\
The act is tortious \textbf{if} there was a violation of someone else's right, \textbf{if} an act/omission is in violation of a duty imposed by law, \textbf{or if} an act/omission is in violation of what according to unwritten law has to be regarded as proper social conduct \textbf{except if} there was a justification for this behaviour.\\
The act can be attributed to him \textbf{if} it results from his fault \textbf{or if} it results from a cause for which he is accountable by virtue of law/generally accepted principles.
\end{quotation}

To further evaluate the formulation of argument diagrams we construct an argument not from text but directly from a diagram. \autoref{fig:evaltort2manual} shows the argument diagram for Dutch tort law from \citet{Verheij:2017gp}.

\begin{sidewaysfigure}[p]
    \centering
    \begin{haslpicture}[scale=0.25]{evaltort2manual}
        dut: There is a duty to repair someone's damages
        dmg: Someone has suffered damages by someone else's act
        unl: The act committed was unlawful
        imp: The act can be imputed to the person that committed the act
        cau: The act caused the suffered damages
        vrt: The act is a violation of someone's right
        vst: The act is a violation of a statutory duty
        vun: The act is a violation of unwritten law against proper social conduct
        jus: There exists grounds of justification
        ift: The act is imputable to someone because of the person's fault
        ila: The act is imputable to someone because of law
        ico: The act is imputable to someone because of common opinion
        prp: The violated statutory duty does not have the purpose to prevent the damages
        a: dmg unl imp cau supports dut
        ax: vst prp attacks a
        c: vun supports unl
        d: vst supports unl
        e: vrt supports unl
        dx: jus attacks d
        ex: jus attacks e
        f: ico supports imp
        g: ila supports imp
        h: ift supports imp
    \end{haslpicture}
    \caption{Argument diagram for Dutch tort law, adapted from \citet{Verheij:2017gp}.}
    \label{fig:evaltort2manual}
\end{sidewaysfigure}

Translating the diagram in \autoref{fig:evaltort2manual} to text results in nine formulations. The first is formulation is again the argument interpreted as a direct argument.

\begin{quotation}
There is a duty to repair someone's damages because someone has suffered damages by someone else's act, the act committed was unlawful because the act is a violation of unwritten law against proper social conduct, because the act is a violation of a statutory duty except there exists grounds of justification, and because the act is a violation of someone's right except there exists grounds of justification, the act can be imputed to the person that committed the act because the act is imputable to someone because of common opinion, because the act is imputable to someone because of law, and because the act is imputable to someone because of the person's fault, and the act caused the suffered damages except the act is a violation of a statutory duty.
\end{quotation}

\noindent The second formulation is more in line with the original law text.

\begin{quotation}
There is a duty to repair someone's damages if someone has suffered damages by someone else's act, the act committed was unlawful, the act can be imputed to the person that committed the act, and the act caused the suffered damages unless the act is a violation of a statutory duty and the violated statutory duty does not have the purpose to prevent the damages.\\
The act committed was unlawful if the act is a violation of unwritten law against proper social conduct, if the act is a violation of a statutory duty unless there exists grounds of justification, or if the act is a violation of someone's right unless there exists grounds of justification.\\
The act can be imputed to the person that committed the act if the act is imputable to someone because of common opinion, if the act is imputable to someone because of law, or if the act is imputable to someone because of the person's fault.
\end{quotation}

\noindent The remaining seven are all variants of the indirect argument where only the use of `except if' and `unless' vary.

\subsubsection{Toulmin}
Like HASL/0 and HASL/1, the grammar of HASL/2 cannot parse example~\ref{evaltoulanne}. It can interpret example~\ref{evaltoulannenat} but does so ambiguously again (\autoref{fig:h2evaltoulannenat}) because it cannot distinguish between minor and major claims in text.

\subsection{Figures of interpretations}
\input{evaluation/figures}

\paragraph{Toulmin}
