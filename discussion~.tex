% To sum up (yes, this is still mainly for myself as a reminder)
% \begin{enumerate}
% \item General: Context necessary for ruling out possible targets of the attack.
% \item Petersen: Toulmin's backing can be expressed. In a single sentence.
% \item Petersen: enthymeme resolution shows the difference between `will not be' and `can be taken almost certainly'.
% \item Grammar rules exclude each other. The rule for the undercutter excludes the rule for missing major premise.
% \item Enthymeme resolution for attacks would have been really helpful. (in the case of `Tweety can fly but Tweety is a penguin')
% \item Enthymeme resolution which allows for assumed claims to be attacked would have been really helpful since this is a natural thing to do (e.g. in a straw man fallacy)
% \item All elements of Toulmin's model except for the qualifier can be modelled. Explicitly modelling the qualifier may not always be necessary.
% \end{enumerate}

Todo: This structure is then drawn in a similar way as normal argumentation, although there is a slight difference in meaning. In specific arguments, when a relation is stated, it is claimed to be there. In general arguments, or rules, a condition is only a statement that there might be such a claim, and that it would lead to a certain conclusion. This arguing happens at a different level (the level of the general claim) than the level where the rules are applied (the level of the specific claim) and as such merging the two in the same space in the argument diagram might not be the best solution.


\subsection{Anaphora resolution}
HASL/1 implements anaphora resolution as one of the stages during parsing. It does this by linking names, nouns and pronouns to each other when combining parts of the parse tree. Every partial parse forms a partial argument, which, among a list of claims and relations, keeps a list of all entities it has encountered and linked to each other. The linking is done using the heuristics described in \autoref{sec:anaphoraresolution}.

The resulting algorithm is similar to Hobbs' algorithm \citep{Hobbs:1978kh} which is regarded as a computationally simple but effective approach and is often referred to as baseline for more developed approaches in pronominal anaphora resolution \citep{Lappin:1994uv,Mitkov:1999ux}.



\paragraph{A lack of common knowledge makes for curious connections.}
There is no dictionary or other form of common knowledge backing the linking process. As a result, HASL will connect `she' with `the king' as soon as `the king' has not yet been linked to another pronoun and occurred before `she' in the sentence.

\paragraph{Linking anaphora during argument construction is not the right moment.} This problem can best be illustrated using an example, \autoref{fig:descanaphora}. In this example, both Tweety and Ducky have wings, but the name Tweety only occurs in the top-most claim. The two sentences that contain the anaphora, both `he has wings', are part of the supports, which are joined together before they are connected to the `Tweety can fly' claim. During this connection the `he' in both `he has wings' claims are connected, and as the claim `he has wings' is seemed to be a duplicate, it is dropped. If the parse tree were slightly different, such that the basic element is not a claim but a claim with support, and additional supports are then linked to this combination, anaphora resolution for sentences such as these would be more successful.

\begin{figure}[p]
	\centering
	\begin{subfigure}[t]{0.45\textwidth}
		\centering
		\begin{haslpicture}[scale=0.45]{descanaphora0}
			c: Ducky (#15/11) has wings
			d: Ducky (#15/6) is a bird
			e: Tweety (#1/1) can fly
			f: d supports e
			g: c supports e
			h: c supports d
		\end{haslpicture}
		\caption{Current interpretation by HASL/1}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\textwidth}
		\centering
		\begin{haslpicture}[scale=0.45]{descanaphora1}
			a: Tweety can fly
			b: Tweety has wings
			c: Ducky is a bird
			d: Ducky has wings
			1: b c supports a
			2: d supports c
		\end{haslpicture}
		\caption{Ideal interpretation}
	\end{subfigure}

	\begin{subfigure}[t]{0.45\textwidth}
		\resizebox{\linewidth}{!}{%
		\begin{tikzpicture}
			\Tree [.argument
				[.claim \edge[roof]; {Tweety can fly} ]
				[.supports
					[.support
						[.argument
							[.claim \edge[roof]; {he has wings} ] ] ]
					[.support
						[.argument
							[.claim \edge[roof]; {Ducky can fly} ]
							[.supports
								[....
									[.claim \edge[roof]; {he has wings} ] ] ] ] ] ] ]
		\end{tikzpicture}
		}
		\caption{Current parse tree of HASL/1}
	\end{subfigure}
	\begin{subfigure}[t]{0.45\textwidth}
		\resizebox{\linewidth}{!}{%
		\begin{tikzpicture}
			\Tree [.argument
				[.argument
					[.claim \edge[roof]; {Tweety can fly} ]
					[.support
						[.argument
							[.claim \edge[roof]; {he has wings} ] ] ] ]
				[.support
					[.argument
						[.claim \edge[roof]; {Ducky can fly} ]
						[.support
							[.argument
								[.claim \edge[roof]; {he has wings} ] ] ] ] ] ]
		\end{tikzpicture}
		}
		\caption{Ideal parse tree for anaphora resolution}
	\end{subfigure}

	\haslsentence{Tweety can fly because he has wings and because Ducky is a bird because he has wings.}
	
	\caption{Example of anaphora resolution gone wrong due to the wrong moment of linking.}
	\label{fig:descanaphora}
\end{figure}

\paragraph{The concept of scoping.}
Anaphora resolution is challenging when certain parts of the text are conditional, i.e. the general claims. Humans can read the generalized claims and interpret them as a scenario, resolving any of the individuals or a made-up individual as the subject of the general claim, interpreting them as a what-if scenario, and continue from there. HASL has no concept of inference or scenarios and as such tries to limit the anaphora resolution in generalized claims using scopes. A general claim has a scope, and inside that scope anaphora can be linked to each other. They cannot be linked to things outside the scope, with one exception: general claims that themselves are supporting general claims can refer to things in the scope of the supported claim.


\paragraph{Practical reason for creating assumed claims: we need the relation.}
Enthymeme resolution, which HASL does by assuming claims it expects in the argument based on the relations present, has a practical purpose: the schematic representation used cannot contain a warrant without a datum, as the warrant is connected to the relation between datum and conclusion. By assuming a claim as the datum if it isn't explicitly stated this relation is introduced. 

\paragraph{HASL/2 cannot do it because it doesn't do Deep Parsing.}
The enthymeme resolution requires a complete parsing of the claims to determine the parts (i.e. subject, verb, object) it uses to reconstruct the missing claim. HASL/2 does not parse further than the discourse markers, treating claims as just a \todoinline{span of text}. It therefore cannot do enthymeme resolution. It also cannot determine whether it is the general or specific claim that is missing as it cannot differentiate between the two, again due to the lack of a complete parse.
On top of deep parsing, anaphora resolution is required as well, to determine where any pronouns refer to when, for example, deducing which claims an individual needs to adhere to in order to comply with the warrant that is being stated. An example of this being applied is shown in \autoref{fig:assumedata} in \autoref{sec:assumptionconstruction}.

\paragraph{The claim construction algorithm is a heuristic.}


\paragraph{There is no real deep understanding of arguments.}



%non-deductive reasoning and deductive reasoning are two different but equally often occurring forms of reasoning and a reasonable epistemology must accommodate both \cite{Pollock:1987jx}