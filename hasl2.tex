\section{HASL/2: A flexible understanding}
HASL/2 extends the core idea of HASL/1, but tries to overcome three of HASL/1's weaknesses: the underlying English grammar, the limited structure for general claims, and the ability to also use the grammar to formulate argumentative text.

The grammar in HASL/1 is a mix of English language constructions (i.e. how to write a claim) and argumentation constructions (i.e. how to write a support relation between two claims). In the end, we are only interested in the latter. The former is necessary, but also limiting as only claims that can be expressed in this grammar can be used in the construction of arguments. In HASl/2 we leave out the grammar for claims, opting instead for splitting sentences into claims and discourse markers purely on the presence of those discourse markers. This would no longer limit what can be expressed in claims, as HASL/2 will not try to understand those at a level that HASL/1 did.

Additionally HASL/2 has a better formalised structure for major claims: their conditions and exceptions are no longer ad-hoc encoded in the claim itself, but are an integral part of the argument diagram. This allows us to write more expressive general claims, and also understand the construction of these better by expressing them in a similar structure as those of arguments in the same argument diagram.

Finally, the grammar for argumentation itself could be used to also translate an argument diagram to argumentative text, but the implementation of HASL/1, which is build on a limited grammar for English, does not allow for this. In HASL/2 we add the ability to formulate argumentative texts given an argument diagram by applying we use to parse argumentative text in reverse on the argument diagram. This would allow us to use HASL/2 as a tool to explore the understanding of argumentative text, e.g. try to draw an argument diagram and see how this is expressed in text, and then in turn see how these texts can also be interpreted as some constructions can be formulated in different ways, and similarly some argumentative texts can be interpreted in different ways.

\begin{exe}
	\ex\label{ex:h3s1} Tweety can fly because if something is a bird it can fly. 
	\ex\label{ex:h3s2} Tweety can fly because birds can fly.
\end{exe}

\noindent For HASL/1, examples~\ref{ex:h3s1} and~\ref{ex:h3s2} are one and the same argument only formulated differently, but only the latter can be parsed. HASL/2 is able to parse both sentences and create the same argument diagram for each of them, or take that argument diagram and generate at least these two sentences.

The process for parsing text in HASL/2 is similar to that of HASL/1, but simpler. Like in HASL/1, text is first segmented into tokens, and these tokens are then parsed into a structured argument. Different from HASL/1, these tokens are not single words and punctuation, but spans of text: claims are segmented into a single token. Therefore the grammar has to only consist of rules that are relevant to the argumentative structure of the text.

\input{diagrams/hasl2pipeline}

To facilitate these features, HASL/2 makes use of an intermediary representation of arguments: the structural representation. The structural representation can be transformed to the diagram representation used by HASL/1 and for our drawings, but also to the textual representation, i.e. argumentative texts. These three representations of the same argument are shown in \autoref{fig:hasl2representations}.

\input{diagrams/hasl2representations}

\subsection{Segmentation}

HASL/2 cuts the strings of text it receives as input into tokens by separating them using discourse markers \citep{Marcu:1997kn} to segment the text into stand-alone parts (argumentative discourse units, ADU's, or as we call them: claims).

These discourse markers are keywords such as `but', `because', etc. We also use conjunctives such as `and', `or', punctuation, and any other explicitly mentioned token in the grammar. A complete list of all discourse markers used by HASL/2's grammar can be found in \autoref{table:discoursemarkers}. To illustrate, the following listing compares how the sentence `Tweety can fly because birds can fly.' is split into respectively tokens or clauses by HASL/1 and HASL/2.

\begin{itemize}
	\item* HASL/1: `Tweety' `can' `fly' `because' `birds' `can' `fly' `.'
	\item* HASL/2: `Tweety can fly' `because' `birds can fly' `.'
\end{itemize}

\subsection{Data structure}
HASL/2 requires both the description of the structure as well as the interpretation parts of the grammar to be reversible. In HASL/1 the transformation rules that were applied as soon as a grammar rule was finalized and transforms the parsed text segments into a local argument structure are implemented as imperative Python code. This code cannot be run in the opposite direction (from local argument structure to parsed text segments). 

\paragraph{Structural representation predicates}

We construct a limited data structure that implements the functionality we need for this transformation that is bidirectional. This data structure contains several constructs that allows us to map the data from one representation onto the other. It consists of three predicates:

\begin{description}
	\item[slot] The slot primitive maps elements from the textual grammar onto the nested structure of the interpretation part of the grammar and vice versa. The textual grammar is represented as a list (one element for each terminal or non-terminal) and via \texttt{slot($n$)} the $n$th element of that list can be accessed. When reversed (i.e. going from interpretation to a sentence) this maps the structure at that position in the structural representation to be reversed into a textual representation by the grammar for the $n$th non-terminal.
	\item[tlist] The \texttt{tlist}\footnote{Called tlist as the name `list' was already taken by a native structure in Python.} creates a list that consists of a head and a tail, either element can be either a value or one of the predicates like \texttt{slot($n$)}. \texttt{tlist} is used to construct lists in the structural representation, for example when multiple supporting arguments support the same claim.
	\item[template] Using \texttt{template} allows us to structure the parts of the structural representation into data structures described below, and to identify which data structure it has. E.g. a supporting claim is mapped via \texttt{template} to a \emph{Support} structure as described below. When reversing the structural representation, the \texttt{template} primitive will only be reversible if the structure is of the same type. Using \texttt{slot} and \texttt{tlist} we can link values to the properties of the structure (e.g. link the list of supporting claims to the \emph{datums} property of the \emph{Support} data structure.) 
\end{description}

\noindent Using these rules we can create grammar rules such as the following two examples, that can be used in both directions:

\begin{grammar}
<argument> ::= <claim> [ <supports> ] [ <attack> ]
\end{grammar}

\noindent This grammar rule, including transformation function, is implemented using the predicated described earlier. In our Python code, it is expressed in the following way:

\begin{verbatim}
rule('argument',
	['claim', 'supports?', 'attack?'],
	template(Argument, claim=slot(0), supports=slot(1), attack=slot(2))),
\end{verbatim}

\noindent In this example, the first line contains the name of the rule, `argument'. The second line expresses which rules it consists of, i.e. a claim and optionally a list of supports and an attack. The last line shows the transformation function. If this grammar rule is matched, and each of the rules on its right hand side can be filled in (where the optional rules are allowed to be filled in with empty matches) then this transformation function is used to construct the result of this grammar rule.

The \texttt{template} predicate constructs a new \emph{Argument}, which \emph{claim} is then filled in by \texttt{slot(0)} with the result of the first element on the right hand side of the rule, i.e. \texttt{<claim>}. The same is done for \emph{supports} and \emph{attack}. \emph{supports} is a list, and since it is optional it may be an empty list. 

The following example shows how these lists are constructed using the simple example of \texttt{<sentences>} since the actual rule for \texttt{<supports>} also deals with the discourse markers between them, i.e. the comma's and `and'. 

\begin{grammar}
<sentences> ::= <sentence> <sentences>
\end{grammar}

\noindent The rule is translated into our Python format of our implementation as follows:

\begin{verbatim}
rule('sentences',
	['sentence', 'sentences'],
	tlist(head=slot(0), tail=slot(1))),
\end{verbatim}

\noindent In the above example the recursive case of \texttt{<sentences>} rule is defined, which consists of a single sentence followed by itself. The single \texttt{<sentence>} rule, when matched, yields an \emph{Argument} instance, which is then connected to \texttt{slot(0)} as it is the element at index 0. The \texttt{<sentences>} rule yields a \texttt{tlist}, which is connected to \texttt{slot(1)}. As a result, this rule extends that \texttt{tlist} with the \emph{Argument} as head. This way a list of tokens that matches sentences is transformed into a list of Arguments.
	
We extend the data structure with more named elements so that they can be matched with the \texttt{template} primitive described above when reversing an argumentative structure.

\begin{itemize}
	\item Claim
	\begin{itemize}
		\item \textbf{Text}: the text of a claim that is drawn in the box or forms a clause in the textual argument.
	\end{itemize}

	\item Argument
	\begin{itemize}
		\item \textbf{Claim}: the \emph{Claim} that is the conclusion of this branch in the argument.
		\item \textbf{Supports}: a list of \emph{Support} items that each are an argument for supporting the conclusion (i.e. as in independent support).
		\item \textbf{Attack}: an optional \emph{Attack} structure attacking the conclusion of this argument.
	\end{itemize}

	\item Attack
	\begin{itemize}
		\item \textbf{Arguments}: a list of one or more arguments that in a cooperative way attack the conclusion.
	\end{itemize}

	\item Support
	\begin{itemize}
		\item \textbf{Datums}: a list of \emph{Claim}s that together support the conclusion of the argument that this Support structure is a part of.
		\item \textbf{Warrant}: an optional \emph{Argument} that warrants the relation, telling us why it is allowed to interpret the datums as support.
		\item \textbf{Undercutter}: an optional \emph{Argument} that undercuts this support. It does not attack any of the individual claims, but instead argues that the supporting clause is not (or no longer) relevant.
	\end{itemize}

	\item Warrant
	\begin{itemize}
		\item \textbf{Claim}: a \emph{Claim} that is the conclusion of this warrant, e.g. `Something can fly'.
		\item \textbf{Conditions}: a list of \emph{WarrantCondition}s in disjunctive normal form that indicate when the claim of this warrant can be applied to a certain entity. E.g. conditions could be `if something is a bird' and `if something has wings', and generally if one of the claims in the \emph{Datums} attribute of the \emph{Support} structure matches either of those for the subject of the \emph{Claim} of the \emph{Argument} which is supported by that \emph{Support} structure, it should apply.
	\end{itemize}

	\item WarrantCondition
	\begin{itemize}
		\item \textbf{Claims}: a list of \emph{Claim}s in conjunctive normal form (i.e. cooperative support) that indicate when this condition is true, e.g. `something is a bird and something is not a penguin'. These are the conditions that need to be explicitly stated as or being assumed as being the case.
		\item \textbf{Exceptions}: a list of \emph{WarrantException}s in disjunctive normal form (i.e. independent support) indicating under which circumstances the warrant cannot be the case according to this condition. E.g. `something is a penguin' could be a exception. By stating it as an exception instead of a condition it is assumed to not be the case, unless explicitly stated in the argument.
	\end{itemize}

	\item WarrantException
	\begin{itemize}
		\item \textbf{Claims}: a list of \emph{Claim}s in conjunctive normal form. Most often it is just one claim, e.g. `something is a penguin', but it could be that an exception is very specific, and due to the rule-like nature of the argument diagram structure used by HASL/2 we specify all clauses of the exceptions (and conditions) explicitly.
	\end{itemize}
\end{itemize}

The \emph{Warrant} structure mirrors the \emph{Claim}, and the \emph{Support} mirrors the \emph{WarrantCondition}. Only in the latter there is no attribute for a warrant and no room for multiple exceptions: in the case of an supporting argument in a direct claim, one undercutter is enough to stop a supporting argument from being relevant. In the context of a warrant, which is more like the mentioning of a rule, one can state multiple exceptions to the application of that rule. Stating these happens only to inform on their existence, it does not apply them to this particular case. For example, in a sentence like `Tweety can fly because he is a bird except he is a penguin.' there is no need to also state that his wings are too short, just being a penguin is enough to attack the claim that Tweety can fly. In a warrant, the sentence is more rule-like and not directly applied to a specific context: `birds can fly unless they are a penguin or their wings are too short'. It is relevant to state multiple exceptions in this scenario as there may be (we don't know yet) a datum that matches one of these conditions. Additionally, in HASL/2 we use the warrant to describe rule-like text such as a body of law. In such texts, there is no ongoing argument, there is only a structure of when certain claims can be made (e.g. when an act is tortious) and under what exceptional circumstances they can't be stated.

\subsection{Parser}
HASL/2 parses sentences using depth-first search as this algorithm is easier to adapt to use the grammar rules in both directions and is sufficient for demonstration purposes.

We do not identify part-of-speech tags, which we did for HASL/1, as the smallest elements are now spans of text, and we do not analyse these spans in depth. Only discourse markers are identified.

Note that we still explore the full search space. When there are multiple possible rules to match (e.g. multiple rules with the same name) all of these are tried.

Because we now use depth-first search, all grammar rules are only allowed to be right-recursive. Left-recursive rules would cause an infinite search space.

\paragraph{Interpreting}

Each time a token is matched to a literal in the grammar, it is consumed. How the token is processed depends on the literal it matches against. For example, literals that match discourse markers just check whether they match, but literals that match the spans of text between discourse markers (i.e. claims) capture and transform these to a span of text that will eventually be displayed as a box in the argument diagram.

When all elements of a rule are complete, a rule consumes these elements as well. This is where the structure of arguments is created. For example, the grammar rule that matches an \texttt{<argument>}, which is constructed of a claim with one or more supports, creates the \emph{Argument} data structure at this stage.

\paragraph{Formulating}
The reverse grammar works the same way, except that first the top-most data structure is reversed using the rule. I.e. \emph{Argument} is reversed into a list of its components based on the \texttt{<argument>} grammar rule.

\subsection{Grammar}

The grammar rules now consist of a name, the constituents which are names of terminals (discourse markers) and non-terminals (other rules), and a structural representation of the argumentative structure that matches the rule constructed from the primitives described earlier.

Each rule starts with a name, and is then followed by the constituents which refer to other grammar rules, or terminals such as `because' in the second rule. Terminals, which are literals that also serve as the discourse markers, are surrounded by quotes, rules by guilletes.

Some of the rules use square brackets in the antecedent. These sections are optional, and can also match an empty list of tokens. In the parse trees and our implementation these are displayed as a rule name ending with a question mark.

Each rule has an associated transformation function, written using the \texttt{slot}, \texttt{tlist} and \texttt{template} predicates described earlier. In the first rule, when a text matches with the \texttt{<argument>} rule, it yields a structural representation in the form of an instance of \emph{Argument}, which has a property named \emph{claim} that holds the structural representation matched by the \texttt{<claim>} rule in the grammar, and a property \emph{supports} that does the same for \texttt{<supports>}.

We make the distinction between argumentation and rules. In argumentation, a claim is proposed (e.g. Tweety can fly) and directly supported or attacked by other claims (`he is a bird' or `he is a penguin'). In rules, we describe this same logic, but do not apply it directly to a claim that is discussed here and now., e.g. birds can fly unless they are penguins. More often, rules themselves are not the target of the discussion, but they are mainly discussed in whether they are applicable in the argument or not, or whether an explicit or implicit exception to the rule is the case.

We first define the grammar for argumentation. The grammar for rules (encoded in \texttt{<warrant>}) will be defined thereafter.

\paragraph{Arguments}

\begin{grammar}
<sentences> ::= <sentence> <sentences>
	\alt <sentence>

<sentence> ::= <argument> `.'
\end{grammar}

\noindent Arguments are sentences, always with a claim, and optionally with one or multiple reasons in favour of that claim. Optionally there can also be an argument against the claim. 

\begin{grammar}
<argument> ::= <claim> [ <supports> ] [ <attack> ]

<claim> ::= <text>
\end{grammar}

\noindent We allow only a single attacking argument per sentence as this seems natural to do: a sentence seldom contains multiple different attacking arguments, and there is no natural way to write multiple different attacking arguments.

\paragraph{Pros}

% \input{diagrams/hasl2support}

\begin{grammar}
<supports> ::= <support-list>
	\alt <support>

<support-list> ::= <support> `,' <support-list>
	\alt <support> `and' <support>

<support> ::= `because' <arguments> [ `and' <warrant> ] [ <attack-marker> <argument> ]
\end{grammar}

\noindent The \texttt{<supports>} rule allows us to write `because a, because b and because c' like constructions. The referencing of \texttt{<support-list>} and \texttt{<support>} from \texttt{<supports>} is different from the way HASL/1 writes these type of self-referencing constructions as the parser we use utilizes a depth-first search approach. As such, referencing the rule itself as the first constituent would allow it to recur infinitely without making progress, causing it to become stuck in an infinite loop.

\paragraph{Cons}

% \input{diagrams/hasl2attack}

\begin{grammar}
<attack> ::= <attack-marker> <arguments>

<arguments> ::= <argument-list>
	\alt <argument>

<argument-list> ::= <argument> `,' <argument-list>
	\alt <argument> `and' <argument>
\end{grammar}

\noindent For the attacking argument we allow multiple arguments after the discourse marker marking these arguments as attacking. This way we can construct a combined attack from one or more claims, and each of these claims can themselves be supported or attacked.

\begin{grammar}
<attack-marker> ::= `but' | `except' [ `that' ]
\end{grammar}

\noindent We separate the discourse markers that mark an attack using \texttt{<attack-marker>} to allow us to use different discourse markers. This list can be extended, but these (`but', `except' and `except that') are the most common and sufficient for our examples.

\paragraph{Rules}
% \input{diagrams/hasl2warrant}

We have not yet defined the \texttt{<warrant>} rule referenced by the \texttt{<support>} rule. The warrant can be interpreted as the major argument in a syllogism, or the warrant in Toulmin's argument model. The warrant is a general rule, or a generalization, e.g. `birds can fly', or a rule with explicit conditions, e.g. `an act is unlawful if it violates someone's rights, if it violates an written or unwritten law or if it violates a statutory duty'.

\begin{grammar}
<warrant> ::= <claim> [ <conditions> ]
	\alt <claim> <exceptions>
\end{grammar}

\noindent A warrant can consist of a claim with one or more conditions where each condition is a separate reason for the claim part of the warrant to be applicable, e.g. something can fly if it is a plane or if it is a bird.

A warrant can also be just a claim (e.g. `birds can fly') or a claim with only exceptions `birds can fly except when they are penguins'.

\begin{grammar}
<conditions> ::= <condition-list> `or' <condition>
	\alt <condition>

<condition-list> ::= <condition-list> `,' <condition>
	\alt <condition>

<condition-marker> ::= `if' | `when'

<condition> ::= <condition-marker> <claims> [ <exceptions> ]
\end{grammar}

\noindent Conditions themselves can have multiple claims. This to write sentences that form a condition in a combined claim like structure, e.g. when all of the claims need to be the case for the condition to be applicable. Optionally each (combined) condition can have one or more exceptions: cases in which all of the claims in the condition are true but when the conclusion is not applicable.

\paragraph{Exceptions} 

Exceptions can partially be interpreted as conditions that must not be true, with the difference that, like in default logic, the claims in conditions need to be explicitly the case while the exception is implicitly not the case, unless it is mentioned elsewhere.

\begin{exe}
	\ex\label{ex:h3i1} Something can fly when it has wings and it has an engine or when it is a bird.
	\ex\label{ex:h3i2} Something can fly when it has wings or when it is a bird unless it is a penguin or its wings have been clipped.
\end{exe}

In contrast to the undercutter in a supporting argument, a warrant can have multiple exceptions. In the indirect case, there is a need to be able to mention all the exceptions to a rule since there no direct evaluation: the avenue in the argument doesn't end as soon as the exception is mentioned, as it is only a hypothetical case.

\begin{grammar}
<exceptions> ::= `unless' <unmarked-exceptions>
	\alt `except' <marked-exceptions>
\end{grammar}

Exceptions come in two types, marked and unmarked. These are just to differentiate between writing `unless a or b' and `except if a or if b', where in the latter the `if' (or `when') is repeated with each exception to the rule. They are interpreted in the same way.

All in all, the examples~\ref{ex:h3i1} and~\ref{ex:h3i2} should be interpreted as shown in \autoref{fig:h3i2}.

\input{diagrams/h3ex1}

\begin{grammar}
<unmarked-exceptions> ::= <unmarked-exception-list> `or' <unmarked-exception>
	\alt <unmarked-exception>

<unmarked-exception-list> ::= <unmarked-exception-list> `,' <unmarked-exception>
	\alt <unmarked-exception>

<unmarked-exception> ::= <claims>

<marked-exceptions> ::= <marked-exception-list> `or' <marked-exception>
	\alt <marked-exception>

<marked-exception-list> ::= <marked-exception-list> `,' <marked-exception>
	\alt <marked-exception>

<marked-exception> ::= <condition-marker> <claims>

\end{grammar}

\subsection{Structured arguments and argument diagrams}

Each argument has three representations, shown in \autoref{fig:h3ex2}: The parsing of a sentence using the described grammar yields a parse tree. For each branch in the tree the transform function of the rule associated with that branch is applied (the transform function describes the mapping from the rule's antecedents to a data structure using the \emph{slot}, \emph{tlist} and \emph{template} primitives).

\input{diagrams/h3ex2}

The resulting data structure, the structural representation, is a stricter representation of the argument. For example, it describes that a top-level argument consists of a single claim, which itself can only be text, with optionally multiple supporting arguments and an attacking argument. A supporting argument can have one or more sources, but optionally only a single warrant. It describes the rule-structure of the warrant, etc.

This opposed to the diagram representation of the argument, which can only describe boxes (claims) and arrows (relations), with the option for arrows to originate from multiple boxes as a way to encode combined support/attack relations.

Transitioning between the structural representation and the diagram representation can be done in both directions for arguments that fit both representations.

\paragraph{Structured to diagram}
For example, to construct an diagram representation from a structural representation of an argument, we can simply create relations for each of the supporting arguments of the argument, as well as create an attack relation if there is an attacking argument.

\paragraph{Diagram to structured}
To transition in the opposite direction, we first find the top-most claims in the argument diagram, and for each of these we identify the supporting and attacking arguments by following the relations in the diagram representation. 

If there are multiple attacking arguments for an argument, a choice needs to be made, as this cannot be described in the structured representation of an argument. We account for this difference by creating multiple structured representations (i.e. multiple sentences) with each of them one of the attacking arguments. Only the first sentence will have the supporting arguments as there is no way to represent the grouping (which attacking argument should be grouped near which supporting arguments) in the diagram representation.

Other structures in the diagram representation, such as relation which targets the relation between the warrant and the attack or support relation the warrant supports, cannot be represented in the structured representation. Even in the diagram representation it has no meaning, but that structure is allowing enough to encode it. These feature of an argument that cannot be represented in the structured representation are lost in the transition.

\subsection{Generating textual arguments}

The generation of text from an argument diagram also occurs in two stages (see \autoref{fig:hasl2pipeline}). First it converts the diagram representation to a structural representation. This conversion yields a list of \emph{Argument} structures, one for each sentence. Then the parser applies the grammar rules in a depth-first search on this structural representation. For each rule, the transform function is applied in reverse. If this succeeds, e.g. if the rule describes an \emph{Attack} structure and also finds such a structure at that position in the structural representation, the search continues according to the antecedents of the grammar rule. If it does not, this search avenue is skipped and the next rule (i.e. a different rule with the same name) is tried. As the parser does an exhaustive search, all search avenues are tried, hence if there are multiple solutions for a rule, each of them is once applied in one of the results of this stage. For example, as there are three ways to fill in the \texttt{<attack-marker>} rule, each attack yields three results, each with another attack marker.

The results of this reverse application of the parser are lists of tokens. These can be concatenated together to form sentences.

\input{hasl2implementation}

\input{hasl2evaluation}

\input{hasl2discussion}