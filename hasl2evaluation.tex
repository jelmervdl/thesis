\subsection{Evaluation}
We begin with evaluating HASL/2 using the same tests as HASL/1, listed in \autoref{sec:appevaluation}. The complete evaluation of HASL/2 on these examples is listed in \autoref{table:haslevaluation}. Here we highlight the observations from that evaluation.

\subsubsection{Pros}
\begin{exe}
	\exi{\ref{e130}} Tweety can fly because he has wings and he has feathers and because he is a bird.
	\exi{\ref{e23}} Tweety can fly because Tweety is a bird because Tweety is a duck and because Tweety has wings.
\end{exe}

\noindent HASL/2 has a slightly different approach to capturing cooperative and independent support than HASL/1. Note that HASL/2 cannot determine the difference between a general and a specific claim, and does not make this distinction. As a result, like with HASL/1 and the attack relations, it does not make a choice and presents both interpretations. In \autoref{fig:hasl2e130} and in \autoref{fig:hasl2e41} this becomes visible.
The only option in which this does not occur is when the last claim of a claim list is in the form of a claim that can only be interpreted as a general claim according to the \texttt{<warrant>} rule, i.e. if it contains explicit conditions.

\input{diagrams/hasl2e130}

HASL/2 does not limit the nesting in its supporting or attacking arguments. So while HASL/1 interpreted example~\ref{e23} as Tweety having wings supporting Tweety being a bird, HASL/2 also interprets it as a claim supporting the conclusion `Tweety can fly' (\autoref{fig:hasl2e23}).

\input{diagrams/hasl2e23}

\subsubsection{Cons}
\label{sec:hasl2evaluationcons}
Attacking arguments are more difficult to place correctly in the diagram as was earlier observed in HASL/1.

\begin{exe}
	\exi{\ref{e32}} Tweety can fly because Tweety is a bird but Tweety is a penguin.
	\exi{\ref{e123}} Tweety can fly because birds can fly but Tweety is a penguin.
	\exi{\ref{e110}} Birds can fly but Tweety can not fly because Tweety is a penguin.
	\exi{\ref{e27}} Tweety can fly but Tweety is a penguin but Tweety is a cat.
\end{exe}

\noindent The distinction between attacking the main claim, the supporting claim, or the relation between those two cannot be determined from the grammar. This is also what we see when we parse example~\ref{e32} with HASL/2. All three possible interpretations of the attack are shown in the three diagrams in \autoref{fig:hasl2e32}.

\input{diagrams/hasl2e32}

\input{diagrams/hasl2e123}

In example~\ref{e123} (\autoref{fig:hasl2e123}) there is only one supporting claim, but it is a general claim. HASL/1 is able to interpret this as an enthymeme where this claim is the major premise, and the minor premise is missing. HASL/2's grammar rules specifically expect there to be at least one minor premise in a support argument, hence even though the claim `birds can fly' is a general rule, it is interpreted and only interpreted as a minor premise in \autoref{fig:hasl2e123}.

For example~\ref{e110} HASL/2 has only one interpretation, shown in \autoref{fig:hasl2e110}. This interpretation is as expected.

Example~\ref{e27} shows a difference with HASL/1. In HASL/2, the attack can only attack the previous claim. There is only one parse for this sentence, shown in \autoref{fig:hasl2e27}.

\input{diagrams/hasl2e110e27}

\subsubsection{Warranted support}
As already seen, HASL/2 does not make the distinction between specific and general claims. As a result, it cannot determine based on the words in the claim whether a supporting claim should be interpreted as a claim supporting the conclusion, or a claim supporting the relation, supporting the support of the conclusion.

\begin{exe}
	\exi{\ref{e41}} Tweety can fly because birds can fly and he is a bird.
\end{exe}

\noindent Sentence~\ref{e41}, shown in \autoref{fig:hasl2e41}, is unambiguous in its intentions. HASL/2 however cannot determine whether this is cooperative support, or warranted support.

\input{diagrams/hasl2e41}

\subsubsection{Rules}
\label{sec:hasl2evaluationrules}
Compared to HASL/1 the grammar for HASL/2 regarding rules has been expanded. We can now also formulate rules with explicit conditions, as they occur often in law, and with explicit exceptions.

\begin{exe}
	\exi{\ref{e76}} Something can fly when it has wings and it has an engine or when it is a bird.
	\exi{\ref{e80}} Something can fly if it has wings except when its wings are too short, when they are clipped or when they are broken.
\end{exe}

Example~\ref{e76}, shown in \autoref{fig:hasl2e76} shows how such a rule is interpreted. Example~\ref{e80} (\autoref{fig:hasl2e80a}) shows how these rules can also contain exceptions. However, the grammar is ambiguous and cannot distinguish between reading multiple exceptions, or reading multiple conditions of which one is an exception (\autoref{fig:hasl2e80b}).

\input{diagrams/hasl2e76}

\input{diagrams/hasl2e80}

\subsubsection{Arguments with rules}

\begin{exe}
	\exi{\ref{e91}} Socrates is mortal because he is a man and something is mortal if something is a man.

	\exi{\ref{e93}} Socrates is mortal because he is a man and something is mortal if something is a man or if something is a mammal.
\end{exe}

In example~\ref{e91} the major premise from example~\ref{evalsyl1} is changed to an explicit rule. This is interpreted as shown in \autoref{fig:hasl2e91}. Example~\ref{e93}, interpreted in \autoref{fig:hasl2e93} shows this extended further.

\input{diagrams/hasl2e91e93}

\paragraph{Tort}
A more real life example is based on the translation of \citet{Betlem:1993} of Dutch Tort law: Article 6:162 Definition of a `tortious act' (unlawful act). The text in example~\ref{e127} has been adapted from the quoted source to form self contained claims. Sentences like `As a tortious act is regarded ... ' are simplified to have their structure be presented more clearly: `an act is tortious if ...'.

\begin{exe}
	\exi{\ref{e127}} A person must repair the damages if he committed an act against another person, the act is tortious, the act can be attributed to him and the other person has suffered the damage as a result thereof.\\
	The act is tortious if there was a violation of someone else's right, if an act or omission is in violation of a duty imposed by law or if an act or omission is in violation of what according to unwritten law has to be regarded as proper social conduct unless there was a justification for this behaviour.\\
	The act can be attributed to him if it results from his fault or if it results from a cause for which he is accountable by virtue of law or generally accepted principles.
\end{exe}

\noindent The interpretation of this text is shown \autoref{fig:hasl2e127}. The intended interpretation is shown in \autoref{fig:tort}. The two cannot be compared one to one since our interpretation (\autoref{fig:hasl2e127}) is constructed from taking segments of a text while the reference diagram (\autoref{fig:tort}) is constructed from manual interpretation of the original law text and contains more exceptions. If we disregard these differences, only the exception for justification is different. This is something we cannot easily express in HASL/2's grammar, except by repeating it for each of the situations.

\input{diagrams/hasl2e127}

\input{diagrams/tort}

\subsubsection{Formulating argumentative text}
\label{sec:hasl2evaluationformualtion}
HASL/2 can transform argument diagrams into text by applying the grammar rules in reverse.

\paragraph{Support and Attack}
Let us take the three argument diagrams in \autoref{fig:hasl2e32} as basis for exploring HASL/2's formulation of arguments.

\autoref{fig:hasl2e32a} is formulated in three sentences. The are all almost the same, but differ in their usage of some discourse markers.

\begin{enumerate}
	\item Tweety can fly because tweety is a bird but tweety is a penguin.
	\item Tweety can fly because tweety is a bird except tweety is a penguin.
	\item Tweety can fly because tweety is a bird except that tweety is a penguin.
\end{enumerate}

\noindent \autoref{fig:hasl2e32b} has five formulations. The three listed above, as well as the following two:

\begin{enumerate}
	\item Tweety can fly if tweety is a bird unless tweety is a penguin.
	\item Tweety can fly if tweety is a bird except if tweety is a penguin.
\end{enumerate}

\noindent Again, we see the different discourse markers, but we also see that for the initial three formulations HASL/2 interpreted the top level of the argument diagram as a specific claim, with a reason for it, and an undercutter. In the two new formulations, the argument diagram is interpreted as a general argument, a rule with a condition and an exception.

\autoref{fig:hasl2e32c} has four formulations. Again the three initially presented formulations, and the following:

\begin{enumerate}
	\item Tweety can fly if tweety is a bird.
\end{enumerate}

\noindent This formulation is incomplete and incorrect. This is caused by HASL/2 interpreting the diagram as a general rule, but a condition to a general rule cannot be attacked in HASL/2's grammar. The claim `tweety is a penguin' is lost.

\paragraph{Tort}
If we ask HASL/2 to formulate the diagram presented in \autoref{fig:hasl2e127}, it presents us with 193 different formulations, all equal except for slight variations (i.e. where `but' has been replaced with `except that'). The first formulation is written below.

\begin{quotation}
A person must repair the damages if he committed an act against another person, the act is tortious, the act can be attributed to him and the other person has suffered the damage as a result thereof.\\
The act is tortious if there was a violation of someone else's right, if an act or omission is in violation of a duty imposed by law or if an act or omission is in violation of what according to unwritten law has to be regarded as proper social conduct unless there was a justification for this behaviour.\\
The act can be attributed to him if it results from his fault or if it results from a cause for which he is accountable by virtue of law or generally accepted principles.
\end{quotation}

\noindent If we offer the correct diagram from \autoref{fig:tort} to HASL/2 to formulate, it constructs 3456 formulations, all again the same but with slight alterations in wording. The first of these formulations:

\begin{quotation}
There is a duty to repair someone's damages if someone has suffered damages by someone else's act, the act committed was unlawful, the act can be imputed to the person that committed the act and the act caused the suffered damages unless the act is a violation of a statutory duty and the violated statutory duty does not have the purpose to prevent the damages.\\
The act committed was unlawful if the act is a violation of unwritten law against proper social conduct, if the act is a violation of a statutory duty unless there exists grounds of justification or if the act is a violation of someone's right unless there exists grounds of justification.\\
The act can be imputed to the person that committed the act if the act is imputable to someone because of common opinion, if the act is imputable to someone because of law or if the act is imputable to someone because of the person's fault.
\end{quotation}

% What happens if you try to interpret that text?