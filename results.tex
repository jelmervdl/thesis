\section{Results (or HASL)}
The common theme of HASL is that it provides an interface to turn written arguments into argument diagrams, such as \todo{example diagram 1}. To do this, HASL needs a grammar to parse the textual argument, a data structure to represent the parsed argument, and an argument diagram schema which can visualize the parsed argument.

The sentences that HASL can parse are in English, but since the grammar is limited, there are only a limited number of ways to write the arguments. Often sentences are ambiguious and can be parsed and interpreted in multiple ways. If this is the case for a given sentence, all the possible interpretations are presented.

We have made three iterations of HASL, each one building on the lessons learned from the previous iteration but also making different choices, adding and removing different features.

\paragraph{HASL/0} is an initial implementation of the concept and parses sentences word for word. Words (and interpunction symbols) are the elementary tokens for its grammar. The grammar distincts between specific instances (names, `the' followed by nouns) and general instances to differentiate between specific statements (often used as datum) and general statements (more likely to be a warrant). After parsing a sentence it draws diagrams for each of the interpretations it has found.

\paragraph{HASL/1} extends HASL/0's grammar to treat general rule statements, such as `birds can fly', as actual rules and expands them into `something can fly because it is a bird'. Additionally it tries to resolve anaphora, i.e. link the `it' to `something'. If a sentence has no datum, or no warrant, HASL/1 will assume their existence and add them to the argument diagram. A notable feature of this is that in HASL/1 an unstated assumption can be rebutted.

\paragraph{HASL/2} extends the ideal of HASL/0 to translate between textual and the diagram layout of the argument and provides a grammar that makes these two forms isomorph to each other. It allows a sentence to be parsed into a argument diagram, and an argument diagram to be translated back into a sentence. The grammar still allows for ambiguity \todo{because otherwise the language would be too arfiticial} which results in multiple different translations in either direction. \todo{This undermines the whole idea of isomorph a bit, unless the ambiguity is stable, e.g. all of the sentences result in the same set of diagrams, and each one of the diagrams results in the same set of sentences.}

Each of the versions of HASL has tree parts: the natural language processing part, the internal data structure, and the user facing application. Or: what it can read, what it can understand, and what it can do.

\subsection{Textual arguments}
Todo.

\subsection{Structural arguments}
The basis of the argument diagrams and of the interal data structure of arguments in all iterations of HASL are boxes and arrows \todo{(as seen in Verheij, and Toulmin in a sense)}.

The argument diagrams made with boxes and arrows exist of a few composable elements which can be infinitely repeated to create the argument. These elements are the box: a claim; and two arrows: the support and the attack relation. Claims can attack or support other claims using either of the arrows. An arrow which ends with a triangle indicates support; an arrow ending with a cross indicates an attack. A supporting argument gives reasons in favour of a claim, for example `Tweety is a bird' supports the claim that `Tweety can fly'. An attacking arrow gives a reason to doubt or dispose of a claim, as the claim `Tweety is a penguin' would do to the claim that `Tweety can fly'. In terms of Toulmin's model, the first indicates a datum or a backing, the second a rebuttal.

\begin{figure}
	two mini diagrams:
		Tweety can fly because he is a bird.
		Tweety can fly but he is a penguin.
\end{figure}

Additionally there can also be relations pointing to other relations. A claim supporting a relation should be interpreted as a warrant, an answer to why the claim at the receiving end of that relation can be concluded given the evidence stated in the claim at the source of the relation. Generally this is a more generic statements, e.g. `birds can fly'.
A claim attacking a relation indicates an undercutter, an argument for why the relation may not be the case here, and the claim at the source of the relation is not a valid reason to conclude the claim at the receiving end. These undercutters are often exceptions to general rules, e.g. `pinguins cannot fly'. One can assume that they are not relevant to the argument unless they are explicitly mentioned.

\begin{figure}
	two mini diagrams:
		Tweety can fly because birds can fly and he is a bird.
		Tweety can fly because birds can fly but he is a penguin.
\end{figure}

There are two additional building blocks. First, some claims are supported by multiple independent supporting arguments, when there is more than one datum to assume a claim to be the case. For example, `Tweety is a bird' and `I just saw Tweety fly' are both independent arguments supporting the claim that `Tweety can fly'. If one argument is countered, the other one still supports the claim.
Second, some claims require each other to together form a supporting argument for a claim, for example when multiple conditions need to be met: `Alice committed a tortious act because she owed a duty of care towards Bob and she did not provide.' If either Alice did not owe a duty of care, or she did provide it, the claim that Alice committed a tortious act is no longer supported. In constructions of this type the warrant often is a rule that has multiple conditions, like the definition of a tortious act. 

\begin{figure}
	two mini diagrams:
		Tweety can fly because Tweety is a bird and because I just saw Tweety fly.
		Alice committed a tortious act because she owed a duty of care towards Bob and she did not provide.
\end{figure}

Finally, sometimes a warrant can be applied to a claim without there being a datum; a rule can be applied without needing to prove that the rule is appicable, because in most discussions it is assumed to be always applicable. For example, take the rule or warrant `Anyone is innocent unless proven otherwise.', and the claim `Alice is innocent'. The latter can be directly supported by a supporting arrow that has this warrant as a support itself. The source of the arrow, the place where the datum claim can go, is left empty because here there is no specific datum to support this assumption. When proof of Alice her guilt is found, the application of the warrant is cancelled by an undercutter attacking that empty assumption, because the warrant is no longer applicable. However, the claim in the warrant itself still holds.

With these building components all possible argument diagrams can be constructed. 

\begin{figure}
	\centering
	\begin{haslpicture}[scale=0.5]{argtoulmin}
		a: Claim
		b: Datum
		c: Warrant
		d: b supports a because d
		e: Backing
		f: e supports c
		g: Rebuttal
		h: g attacks a
		i: Undercutter
		j: h attacks d
	\end{haslpicture}
	\caption{All of the components of Toulmin's model can be modelled in HASL's argument diagrams as well.}
	\label{fig:argtoulmin}
\end{figure}

\subsubsection{Data structures}
HASL/0 and HASL/1 share the same data structure, although HASL/1 adds an additional set of supporting structures to keep track of the anaphora.

\paragraph{The basis of HASL/0 and HASL/1} is a graph structure with a list of unique claims and a list of arrows. The parsing happens recursively, so that subsentences can contain complete arguments: an argument is a claim with one or more supporting or attacking relations. Each of these relations can also be sourced in a claim, or an argument, which itself has the option to be supported or attacked. Each argument is represented as a miniature argument graph, and when a sub-argument is parsed, its argument graph is merged into overlapping argument graph by taking the union of both the claim and arrow lists. Note that an arrow is a bit more complex than an edge: an arrow can have multiple source claims (\todo{situation tort}), or non at all (\todo{situation innocent}). Additionally, an arrow can point towards either a claim or another arrow, to allow for warrants and undercutters. Last, every arrow has a type, either supporting or attacking.

The upside of this data structure is that there is need to translate between the graph structure and the argument diagram: they are one and the same representation. The arguments only need to be layed out.

The types of HASL/0:

Argument:
	claims: List[Claim]
	arrows: List[Arrow]

Claim:
	text: str

Arrow:
	type: Enum[Attack,Support]
	source: List[Claim]
	target: Either[Claim,Arrow]

\paragraph{HASL/1} has an additional data structure for the claim itself. It assumes that every claim consists of a subject, verb and object. This structure allows it to deduce assumed warrants from datum-claim pairs, or datums from warrant-claim pairs. \todo{write exact rules for this somewhere else}. The extraction of the subject also allows for anaphora resolution. Together with the textual information about the subject such as the part-of-speech tags and the position in the tree (is it part of the warrant or form the top-most argument) anaphora can in most scenarios be resolved using the rules in \todo{write that list of anaphora resolution rules somewhere}.

In the argument diagram representation none of these additional structures is shown. The claim structure is represented as the full text of the claim, except that the anaphora used in the text are replaced by their most specific resolutions, e.g. `she' is replaced with `Alice', or `he' with `the attacker'. Preference is resolved as follows: proper names > nouns > pronouns.

The assumed supports, such as the assumed datums and assumed warrants, are added as additional claims and arrows to the same graph structure, but are marked as assumed. When merging the graph structure with one from an embedded parse, or the one at the higher level, non-assumed claims and relations are preferred over their assumed counterparts on colission.

Additional types of HASL/1:

Claim:
	subject: Subject
	verb: str
	object: str

Subject:
	name: Optional[str]
	noun: Optional[str]
	pronoun: Optional[str]

\todo{You're missing claim deduplication and the additional structure for scopes..}

\paragraph{HASL/2} uses a different structure to stay more true to the grammatical structure of the sentences. Where in HASL/0 and HASL/1 there is no distinction between warrant and datum in the structure---both are treated as claims with supporting relations---in HASL/3 the warrant has its own structure. The essence of the structure is the same, but the names are different. 
This preservation of the structure allows the grammar to be reversed without needing to look at the context of the structure: to determine whether a claim is a datum or a warrant in the argument diagram, it has to be determined how it is connected to the rest of the diagram. By using different types of structures this step is no longer necessary.

However, this step still needs to be taken as our goal is to make the argument diagram, which does itself not differentiate between different types of claims, the input format for the translation step. HASL/2 goes both ways.

There are some other differences too: Each supporting claim can only have one warrant by design. This isn't a restriction that can be found in the previous iterations of the data structure, but it is always enforced by the grammar. \todo{true? Also with claim deduplication?}

The types of HASL/2:

Claim:
	text: str

Argument:
	claim: Claim
	supports: List[Support]

Support:
	datums: List[Claim]
	warrant: Optional[Argument]
	undercutter: Optional[Argument]

Warrant:
	claim: Claim
	conditions: List[WarrantCondition] in Disjunctive Normal Form

WarrantCondition:
	claims: List[Claim] in Conjunctive Normal Form
	exceptions: List[WarrantException] in Conjunctive Normal Form.

WarrantException:
	claims: List[Claim] # Conjunctive

The Warrant mirrors the Claim, the Support the WarrantCondition. Only in the latter there is no slot for a warrant \todo{since the grammar does not cover that} and room for multiple exceptions: in the case of an supporting argument in a direct claim, one undercutter is enough to stop a supporting argument from being relevant. In the context of a warrant, which is more like the mentioning of a rule, one can state multiple exceptions to the application of that rule. Stating these happens only to inform of their existence, it does not apply them to this particular case. \todo{example would help.}
