% This evaluation, as opposed to the other two for HASL/1 and HASL/2, will show whether my method itself has any value.
\section{General Evaluation}
We have evaluated each of the HASL implementations on their own, to get an idea on whether they behave as expected, and whether the grammar they implement and design choices they are bound to form any limitations, and what these might be. This evaluation was done using the examples in \autoref{sec:appevaluation} and the results are listed in \autoref{table:haslevaluation}.

\autoref{table:goals} lists which requirements we set for HASL, and which are achieved by HASL/1 and HASL/2. This is further elaborated in this section.

\begin{table}[ht]
	\centering
	\begin{tabular}{p{7cm}cc}
		Goal & HASL/1 & HASL/2 \\
		\hline
		Express pros: claims supporting a claim & \cmark{} & \cmark{}\\
		Express cons: claims attacking a claim & \cmark{} & \cmark{}\\
		Express an argument consisting of multiple supporting and attacking claims & \cmark{} & \cmark{} \\
		Combine such expressions into a multiple sentences or a single, more complex sentence & \cmark{} & \cmark{} \\
		Express cooperative and independent support and attack & \cmark{} & \cmark{}\\
		Express warrants and undercutters & \cmark{} & \cmark{} \\
		Express Toulmin arguments & & \\
		Identify warrants & \cmark{} & -\\
		Interpret rule-like claims & \cmark{} & \cmark{} \\
		Anaphora resolution & \cmark{} & -\\
		Enthymeme resolution & \cmark{} & -\\
		Formulate argumentative texts & - & \cmark{} \\
	\end{tabular}
	\caption{Goals of HASL}
	\label{table:goals}
\end{table}

\subsection{Pros}
Both HASL/1 and HASL/2 can express support structures, including cooperative and independent support, as well as chained support.

Independent and chained support can be expressed both by collapsing them into a single sentence in the form of `$A$ because $B$ and because $C$' and `$A$ because $B$ because $C$', as well as by writing separate sentences in the form of `$A$ because $B$. $A$ because $C$.' and `$A$ because $B$. $B$ because $C$.' This gives us the freedom to formulate a human readable text.

Supporting claims is a bit limited in HASL/1, as there are no grammar rules to allow general claims to be supported by general claims. HASL/2 does not have this limitation as it doesn't make the distinction between general claims and specific claims.

\paragraph{Syllogisms}
In HASL/1 and HASL/2 we make the distinction between supporting claims, and supporting relations. The latter serves as a way to model a warrant, a general statement that `warrants' concluding the conclusion based on the claim. This often takes the form of a syllogism, where the warrant is a major premise and the claim the minor premise. We model this in HASL/1 with the general and specific claim.

\begin{exe}
	\exi{\ref{e41}} Tweety can fly because he is a bird and birds can fly.
\end{exe}

This split between general and specific claims based on whether the claim looks like a `general' statement or a `specific' statement works well in HASL/1 as it helps it determine whether a claim has the role of a major or minor premise. For example, example~\ref{e41} makes use of this. In HASL/1 this sentence is interpreted correctly (\autoref{fig:hasl1e41}). In HASL/2, which cannot make the distinction between general and specific claims, this sentence has two interpretations (\autoref{fig:hasl2e41}).

\paragraph{Categorial syllogisms}
The categorical syllogism differs from the syllogism in that there are no claims that match HASL/1's definition of a singular claim: every claim is designating a group or a category.

\begin{exe}
    \exi{\ref{e9}} Birds can fly because birds have wings.
    \exi{\ref{e140}} Birds can fly because birds have wings and creatures with wings can fly.
\end{exe}

Sentences such as example~\ref{e9} and~\ref{e140} cannot be interpreted by HASL/1. For HASL/2 they are no different than other syllogisms of this form, and are interpreted in a similar fashion. One of the goals of HASL, namely escaping the difficulties of language in argument mining by focussing on the structure, is not reachable by HASL/1 due to its dependency on a basic grammar for English.

\subsection{Cons}
All claims and relations can be attacked in HASL, and as such both claims made and the connections that are claimed to be there between claims can be countered. The exception to this are the relations between general claims and their conditions.

The approach of HASL for identifying which claim is attacked is limited because the attacking claim always comes at the end of the sentence. At that moment it can apply to any of the claims previously stated in the last sentence in the case of HASL/2, \autoref{fig:hasl2e27} shows this, or even in any of the previous sentences in the case of HASL/1, visible in \autoref{fig:hasl1e27}.

While support relations can be combined into cooperative and independent relations, attack-relations cannot in HASL/1. Through repetition of the attacked claim independent attacks can be expressed, but cooperative attack is not possible. Supporting the attacking claim, or attacking the attacking claim (i.e. chained attack, which can be described as reinstatement) can be expressed. HASL/2 treats support relations and attack relations equally, and the same grammar rules apply for both.

\subsection{Rules}
In HASL/1 the support for rules as general claims is limited as it does not allow us to express explicitly state conditions for rules. In fact, the way general claims are written in the diagram (i.e. `Something can fly if something is a bird') cannot be parsed by the grammar of HASL/1. This interpretation into this rule-like structure where a general claim becomes a claim with a condition was purely for purpose of enthymeme resolution, and for that purpose this level of detail is sufficient.

In HASL/2 the structure for rules as described in \autoref{sec:rules} is implemented in the grammar. This allows us to capture complex rules, such as tort. To disambiguate this rule structure from the argument structure we draw the condition and exception relations with dashed arrows. To indicate that the whole rule, including its conditions, is treated as a single general claim in the argumentation, the arrow that indicates its use as a warrant is drawn from a dashed box that includes the complete rule and its conditions. In this vision, the way HASL/1 interprets rules with exceptions (cf. as single claims) is correct.

\subsubsection{Anaphora}
Only HASL/1 implements anaphora resolution since HASL/2 does not parse claims and as such is not able to identify pronouns or entities they could refer to in claims.

The anaphora resolution in HASL/1 works well to create more descriptive argument diagrams. For most arguments it works well as it follows the relations in the argument in cooperative and independent support, which works well with the expected interpretation of pronouns. HASL/1 is not able to identify whether or not `she' can refer to `the king' as there is no background knowledge. It is also limited to the subject of claims, as entities and anaphora after the verb are just treated as text, and not as entities that can be linked. In the way anaphora resolution is implemented, it piggybacks on the infrastructure necessary to perform enthymeme resolution. HASL/2 does not implement any form of anaphora resolution. Although the claims in the diagrams are a bit less self-contained, the lack of anaphora resolution has no impact on its ability to identify the argument structure in text.

\subsubsection{Enthymemes}
Resolving enthymemes mainly helps with the understanding of arguments. Only HASL/1 is able to perform this step. For simple claims where only one part of the syllogism is missing (i.e. examples~\ref{e120}, \ref{e121} and~\ref{e122}) this works well except when the connection between the general and specific claim is not made, e.g. when `thieves' and `thief' not treated as the same thing in example~\ref{e47}.

Example \ref{e125}, which was shown in \autoref{fig:hasl1e125} is interesting in that way as the enthymeme resolution takes place deeper inside the argument. It shows how enthymeme resolution can complete the argument and make it easier to understand.

\subsubsection{Formulating argumentative texts}
In formulating HASL/2 uses the collapsing of sentences to formulate a single sentence for complex arguments, as is shown in \autoref{sec:hasl2evaluationformualtion}. The grammar for rules does not allow for this, thus complex rules such as example~\ref{e127}, the excerpt from Dutch Tort law, remain readable.

\subsubsection{Toulmin}
\label{sec:evaltoulmin}
Our argument schema can emulate most elements of Toulmin's model. The datum, warrant and conclusion directly map to our warranted support structure. We do not have a specific element for the qualifier of an argument, and our attacks are specifically attacking a claim in the argument as where Toulmin gives attacking arguments their own place in the argument itself.

We formulated two of Toulmin's example arguments, Examples~\ref{e37} and~\ref{ex:petersen}. Toulmin himself did not write them down in such a precise formulation, but for our purposes we need a purely textual representation. In the first example we intentionally left out the warrant to test whether this is picked up by HASL/1. In the second example we do state the warrant (`a Swede can be taken...'), and support it with a backing (`because the portion of...').

\begin{exe}
    \exi{\ref{e37}} Harry is a British subject because Harry is a man born in Bermuda but Harry has become naturalized.
    \ex\label{ex:petersen} Petersen will not be a Roman Catholic because he is a Swede and a Swede can be taken almost certainly not to be a Roman Catholic because the proportion of Roman Catholic Swedes is less than 2\%.
\end{exe}

\noindent Example~\ref{e37} shows the same ambiguity in interpretation we saw in both HASL/1 and HASL/2: the are three ways to interpret the attack. In two of the three interpretations the warrant is identified.

\input{diagrams/hasl1e37}

In \autoref{fig:hasl1petersen} the interpretations of Example~\ref{ex:petersen} are shown. We have drawn both the interpretations without and with enthymeme resolution to highlight the effect of adding unstated claims to the argument diagram. It shows that although qualifiers are not intentionally taken into account, their meaning is picked up and reflected in the argument by HASL/1: enthymeme resolution does show the difference between `will not be' and `can be taken almost certainly'.

Furthermore the backing is always correctly placed as supporting the warrant.

\input{diagrams/hasl1petersen}

\subsubsection{Tort}
In the concept of HASL, Tort is very similar to categorial syllogisms, in that it is all general claims. In this there is a clear distinction between HASL/1 and HASL/2.

HASL/1 has only a very limited concept of general claims in terms of rules. The major premise of a syllogism is acceptable, but more complex rules are not. HASL/1 only really allows us to express relations using `because'. This is perfect for the application of tort law, but not to describe tort law itself.

HASL/2 can handle general claims, or more specifically conditional claims that are expressed in terms of `if' and `when'. It is less a discussion of support and attack, and more a discussion of potential reasons: conditions and exceptions. These can mostly be expressed in the same argument diagram, as they are in HASL, but they have different grammar for the same structures.

To conclude, HASL/1 is able to understand the application of tort law, but HASL/2 is able to comprehend the law itself. Finally, HASL/2 can comprehend the combination of the law itself, as its application, shown in \autoref{fig:hasl2evaltortapplied}. Unfortunately there is no enthymeme resolution like in HASL/1 to fill in the assumptions made in such an argument.

\input{diagrams/hasl2evaltortapplied}