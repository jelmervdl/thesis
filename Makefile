.PHONY: all clean

all: thesis.pdf

thesis.pdf: thesis.tex introduction.tex background.tex argumentation.tex hasl0.tex hasl1.tex hasl2.tex evaluation.tex discussion.tex
	latexmk -view=pdf ./thesis.tex

clean:
	rm -f *.{aux,log,fls,dvi,bbl,blg} thesis.pdf

open: thesis.pdf
	open thesis.pdf
