#!/bin/bash
set -e

if [ -z "$1" ]; then
	file="thesis.tex"
else
	file="$1"
fi

docker run -v $(pwd):/target jelmervdl/latexmk "$file"
open "${file%.tex}.pdf"
