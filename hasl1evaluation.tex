\subsection{Evaluation}
We experiment with the implemented grammar by trying out different kinds of arguments. We focus on the features we expect HASL/1 to cover, summarized in \autoref{sec:requirements}. We do this by formulating test sentences (listed in \autoref{sec:appevaluation}) and interpreting these with the implementation of HASL/1. The complete results of this evaluation are listed in \autoref{table:haslevaluation} in \autoref{sec:appevaluation}. Here we show our observations by drawing the diagrams for a selection of these sentences.

\subsubsection{Pros}
The examples used throughout this section all already demonstrate that simple claims supporting claims, such as example~\ref{e7}, are interpreted as expected.

\begin{exe}
    \exi{\ref{e7}} Socrates is mortal because Socrates is a man.
    \exi{\ref{e9}} Birds can fly because birds have wings.
\end{exe}

\noindent Because we differentiate between specific and general claims based on whether the subject of the claim refers to something specific or something more general, example~\ref{e9} cannot be parsed by HASL/1. A simple example that combines these into a single sentence is shown in \autoref{fig:hasl1evalconjunctiontweety}.

\input{diagrams/hasl1evalconjunctiontweety}

Combinations of arguments, such as examples~\ref{e18}, \ref{e19} and \ref{e20} supporting each other are also interpreted correctly.

\begin{exe}
\exi{\ref{e18}} Tweety can fly because Tweety is a bird and because Tweety has wings.
\exi{\ref{e19}} Tweety can fly because Tweety is a bird and Tweety has wings.
\exi{\ref{e20}} Tweety can fly because Tweety is a bird because Tweety has wings.
\exi{\ref{e24}} Tweety can fly because Tweety is a bird and Tweety has wings because Tweety is a duck.
\end{exe}

\noindent Example~\ref{e24} is interpreted as shown in \autoref{fig:hasl1e24h}, which is correct according to the grammar, but its interpretation is not the most intuitive one. It would be more intuitive to interpret it as two separate arguments, as drawn in \autoref{fig:hasl1e24i}, incidentally joined through `and' in a single sentence.

\input{diagrams/hasl1e24}

\subsubsection{Cons}
\label{sec:hasl1evaluationcons}
Simple attacks like example~\ref{e10} are interpreted correctly. Mutual attacks such as example~\ref{e15} can be formulated, but are not picked up automatically, even though HASL/1 is able to identify negated claims as such.

\begin{exe}
	\exi{\ref{e10}} Socrates is mortal but Socrates is a god.
	\exi{\ref{e15}} Tweety can fly but Tweety cannot fly.
	\exi{\ref{e27}} Tweety can fly but Tweety is a cat but Tweety has wings but the wings are small.
\end{exe}

\noindent Example~\ref{e27} has five interpretations, shown in \autoref{fig:hasl1e27}. The grammar allows both the left and right side of `but' to be an \texttt{argument} instead of just a \texttt{claim}, which causes the attacking claims to attack each of the previously mentioned claims.

\input{diagrams/hasl1e27}

\subsubsection{Pros and cons}

\begin{exe}
    \exi{\ref{e32}} Tweety can fly because Tweety is a bird but Tweety is a penguin.
    \exi{\ref{e110}} Birds can fly but Tweety can not fly because Tweety is a penguin.
\end{exe}

\noindent In example~\ref{e32} `Tweety is a penguin' is interpreted as a rebutter, an undercutter, and an underminer. Two of these are defensible interpretations, only Tweety is a penguin attacking Tweety is a bird is not.

The interpretation of example~\ref{e32} in \autoref{fig:hasl1e32b} shows that the grammar in the sentence allows for a nonsensical argument: our definition if penguin excludes this interpretation. If HASL would have had access to such world knowledge these interpretations could be excluded.

\input{diagrams/hasl1e32}

Example~\ref{e110} (\autoref{fig:hasl1e110}) shows the attack of a general claim, which itself can be supported. Enthymeme resolution fills in the missing general claim for this support.

\input{diagrams/hasl1e110}

\subsubsection{Undercutter}
Example~\ref{e38} (\autoref{fig:hasl1e38}) shows the classic example of undercutter by Pollock. Again, this argumentative sentence is interpreted by HASL/1 in all three ways. Only the first interpretation (\autoref{fig:hasl1e38a}) is the correct one. The correct interpretation cannot be determined solely based on grammar.

\begin{exe}
    \exi{\ref{e38}} The object is red because the object appears red to me but it is illuminated by a red light.
\end{exe}

\input{diagrams/hasl1e38}

\subsubsection{Warranted support}
General and specific claims are interpreted correctly and placed accordingly in either a role supporting the conclusion, or supporting that support-relation that supports the conclusion.

\begin{exe}
    \exi{\ref{e41}} Tweety can fly because he is a bird and birds can fly.
\end{exe}

\noindent The distinction whether a claim is a general claim or a specific claim is based on the subject of the claim. In example~\ref{e41} this would be `Tweety', which is specific, `birds', which is general, and `he', which is specific again. Sentence~\ref{e41} is interpreted correctly, as shown in \autoref{fig:hasl1e41}.

\input{diagrams/hasl1e41}

\begin{exe}
	\exi{\ref{e44}} Tweety can fly because he is a bird and thieves are punishable.
\end{exe}

\noindent HASL/1 does not check whether the general claim that serves as warrant fits with the specific claim. For example, in example~\ref{e44} a general claim that is not related to the argument at hand is mentioned, and it is interpreted (\autoref{fig:hasl1e44a}) as the warrant for the support relation. The argument does not make much sense, but HASL/1 has interpreted what was written.

The enthymeme resolution causes a second interpretation of example~\ref{e44}. This interpretation (\autoref{fig:hasl1e44b}) adds the assumption that `Tweety is a thief' based on our usage of the general claim here, and the intermediate conclusion that `Tweety is punishable' for similar reasons. From this it also assumes that something can fly if something is punishable.

\input{diagrams/hasl1e44}

\subsubsection{Anaphora resolution}
\begin{exe}
\exi{\ref{e62}} The queen can rule because she is born in a royal family and because the king abdicated.
\exi{\ref{e63}} The queen can rule because the king abdicated and because she is born in a royal family.
\exi{\ref{e71}} The queen can rule because the king abdicated and she is born in a royal family.
\end{exe}

\noindent Anaphora resolution (also shown in the other examples) works for most occurrences of pronouns that occur in the subject of claims, referring to other entities that also occur in the subject of claims. Examples~\ref{e62} and~\ref{e63} work as expected. In example~\ref{e71} `she' is connected to `the king' as there is no knowledge available on the gender of nouns or pronouns.

\subsubsection{Enthymeme resolution}
\label{sec:hasl1evalenthymemeresolution}
Syllogisms always consist of three elements, but not all of them have to be explicitly stated. Example~\ref{evalsyl1} is the complete syllogism, and examples~\ref{e120},~\ref{e121} and~\ref{e122} are the enthymemes. These are all interpreted correctly, and the missing claim is filled in.

\begin{exe}
    \ex\label{evalsyl1} Socrates is mortal because he is a man and men are mortal.
    \exi{\ref{e120}} Socrates is mortal because men are mortal.
    \exi{\ref{e121}} Socrates is mortal because he is a man.
    \exi{\ref{e122}} Socrates is a man and men are mortal.
\end{exe}

\begin{exe}
    \exi{\ref{e125}} Tweety can fly but Tweety is a penguin and penguins cannot fly.
    \exi{\ref{e123}} Tweety can fly because birds can fly but Tweety is a penguin.
\end{exe}

Example~\ref{e125} (shown in \autoref{fig:hasl1e125}) is interesting because the enthymeme is better hidden. Here, the intermediate conclusion that Tweety cannot fly is not explicitly mentioned. This intermediate conclusion is identified and filled in, and becomes the actual rebutter to the claim that Tweety can fly.

\input{diagrams/hasl1e125}

In example~\ref{e123} (shown in \autoref{fig:hasl1e123}) `Tweety is a bird' is assumed because of the general claim. `Tweety is a penguin' attacks either the general claim `birds can fly' (\autoref{fig:hasl1e123b}) or `Tweety can fly' (\autoref{fig:hasl1e123a}), while it should undercut the relation between `Tweety is a bird' and `Tweety can fly' (\autoref{fig:hasl1e123c}).

\input{diagrams/hasl1e123}


